package Entities;

import android.view.View;

/**
 * Created by leuks on 08/07/2016.
 */
public interface Accord {
    public void timerNotify();
    public void rulesNotify();
}
