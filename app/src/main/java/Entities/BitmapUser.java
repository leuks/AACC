package Entities;

import android.graphics.Bitmap;

/**
 * Created by leuks on 08/07/2016.
 */
public interface BitmapUser {
    public void setBitmap(Bitmap bitmap);
}
