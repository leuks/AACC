package Entities;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * Created by leuks on 03/08/2016.
 */
public class EmailTask extends AsyncTask<Void,Void,Void> {
    private Activity act;
    private File fileToSend;
    private String emailToSend;
    private boolean mode;

    public EmailTask(Activity act, File fileToSend, String emailToSend, boolean mode) {
        this.act = act;
        this.fileToSend = fileToSend;
        this.emailToSend = emailToSend;
        this.mode = mode;
    }


    @Override
    protected Void doInBackground(Void... params) {
        //check connection
        ConnectivityManager cm =
                (ConnectivityManager) act.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo == null || !netInfo.isConnectedOrConnecting()) {
            return null;
        }

        final String username = "aaccsender@gmail.com";
        final String password = "aacc10000";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress("aaccsender@gmail.com"));
            message.setRecipients(MimeMessage.RecipientType.TO,
                    InternetAddress.parse(emailToSend));

            if(mode) message.setSubject("Compte rendu de séance");
            else message.setSubject("Exportation historique");

            message.setText("");
            Multipart multipart = new MimeMultipart();

            MimeBodyPart messageBodyPart = new MimeBodyPart();
            FileDataSource source = new FileDataSource(fileToSend.getAbsolutePath());
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(fileToSend.getName());
            multipart.addBodyPart(messageBodyPart);

            message.setContent(multipart);

            Transport.send(message);

            //default

            if(mode){
                message.setRecipients(MimeMessage.RecipientType.TO,
                        InternetAddress.parse("rapport@aacc77.fr"));
            }
            else{
                message.setRecipients(MimeMessage.RecipientType.TO,
                        InternetAddress.parse("statistique@aacc77.fr"));
            }

            Transport.send(message);

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }

        return null;
    }
}
