package Entities;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.app.aacc.R;

import org.apache.harmony.awt.datatransfer.DataSource;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * Created by leuks on 04/07/2016.
 */
public class Tools {
    public static void changeStatusEdit(boolean choice, EditText element, Context context){
        if(choice){
            element.setBackgroundColor(context.getResources().getColor(R.color.error));
            element.setTextColor(context.getResources().getColor(R.color.error_text));
        }
        else{
            element.setBackgroundColor(context.getResources().getColor(R.color.normal));
            element.setTextColor(context.getResources().getColor(R.color.normal_text));
        }
    }

    public static void changeStatusView(boolean choice, View element, Context context){
        if(choice){
            element.setBackgroundColor(context.getResources().getColor(R.color.error));
        }
        else{
            element.setBackgroundColor(context.getResources().getColor(R.color.normal));
        }
    }

    public static void changeSpecialView(boolean choice, View element, Context context){
        if(choice){
            element.setBackgroundColor(context.getResources().getColor(R.color.special));
        }
        else{
            element.setBackgroundColor(context.getResources().getColor(R.color.normal));
        }
    }

    public static void textOut(Context context, String message){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void textOutLong(Context context, String message){
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static Date stringToDate(String dateStr){
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        try {
             date = format.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

}
