package bdd;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by leuks on 03/07/2016.
 */
public class DBManager extends OrmLiteSqliteOpenHelper {
    private static final String DATABASE_NAME = "final.db";
    private static final int DATABASE_VERSION = 1;

    private Dao<Game, String> daoGame = null;
    private Dao<User, String> daoUser = null;
    private Dao<Person, String> daoPerson = null;
    private Dao<Stage, String> daoStage = null;
    private Dao<Room, String> daoRoom = null;

    private User currentUser;

    public DBManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            Log.i(DBManager.class.getName(), "onCreate");
            TableUtils.createTableIfNotExists(connectionSource, Game.class);
            TableUtils.createTableIfNotExists(connectionSource, User.class);
            TableUtils.createTableIfNotExists(connectionSource, Person.class);
            TableUtils.createTableIfNotExists(connectionSource, Stage.class);
            TableUtils.createTableIfNotExists(connectionSource, Room.class);
        } catch (SQLException e) {
            Log.e(DBManager.class.getName(), "Can't create database", e);
            throw new RuntimeException(e);
        }

    }
    // main
    public int testExists(String login, String password) throws SQLException {
        List<User> users = daoUser.queryForEq("login", login);
        if(users.size() > 0){
            User user = users.get(0);
            if(user.getPassword().equals(password)){
                currentUser = user;
                return 0;
            }
            else return 2;
        }
        else return 1;
    }

    //AppManager
    public int createPerson(Person person){
        try {
            List<Person> persons = daoPerson.queryForEq("email", person.getEmail());
            if(persons.size() == 0){
                daoPerson.createIfNotExists(person);
                Log.i("create", "person created");
                return 0;
            }
            else{
                Person personBD = persons.get(0);
                if(personBD.getEmail().equals(person.getEmail())) return 3;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public int createUser(User user){
        try {
            List<User> users = daoUser.queryForEq("login", user.getLogin());
            if(users.size() == 0){
                daoUser.createIfNotExists(user);
                Log.i("create", "person created");
                return 0;
            }
            else{
                User userBD = users.get(0);
                if(userBD.getLogin().equals(user.getLogin())) return 1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public void createOrUpdateStage(Stage stage){
        try {
            daoStage.createOrUpdate(stage);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        for(Room room : stage.getRooms()){
            try {
                daoRoom.createOrUpdate(room);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void deletePerson(Person person){
        try {
            daoPerson.delete(person);
            Log.i("delete", "person deleted");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteUser(User user){
        try {
            daoUser.delete(user);
            Log.i("delete", "user deleted");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int getLastStageId(){
        QueryBuilder<Stage, String> qb = daoStage.queryBuilder();
        qb.orderBy("id", false);
        qb.limit(1L);
        int max=0;
        try {
            max = daoStage.queryForFirst(qb.prepare()).getId();
        } catch (SQLException e) {
            e.printStackTrace();
        }catch (NullPointerException ex) {
            ex.printStackTrace();
        }
        return max+1;
    }

    public int getLastRoomId(){
        QueryBuilder<Room, String> qb = daoRoom.queryBuilder();
        qb.orderBy("id", false);
        qb.limit(1L);
        int max=0;
        try {
            max = daoRoom.queryForFirst(qb.prepare()).getId();
        } catch (SQLException e) {
            e.printStackTrace();
        }catch (NullPointerException ex) {
            ex.printStackTrace();
        }
        return max+1;
    }

    //ImageGame
    public HashMap<String, String> getParameters(String gameName){
        HashMap<String, String> response = new HashMap();
        try {
            Game game = daoGame.queryForEq("name", gameName).get(0);
            String params =  game.getParameters();
            String[] paramsArray = params.split("/");
            switch(gameName){
                case Game.IMAGE:
                    response.put("time", paramsArray[0]);
                    response.put("score", paramsArray[1]);
                    break;
                case Game.SIGN:
                    response.put("total_time", paramsArray[0]);
                    response.put("score_good", paramsArray[1]);
                    response.put("score_bad", paramsArray[2]);
                    response.put("display_time", paramsArray[3]);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return response;
    }

    public void setParameters(String gameName, String parameters){
        switch (gameName){
            case Game.IMAGE:
                try {
                    Game game = daoGame.queryForEq("name", gameName).get(0);
                    game.setParameters(parameters);
                    daoGame.update(game);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            case Game.SIGN:
                try {
                    Game game = daoGame.queryForEq("name", gameName).get(0);
                    game.setParameters(parameters);
                    daoGame.update(game);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    //others
    public void initDao(){
        getDaoGame();
        getDaoPerson();
        getDaoStage();
        getDaoUser();
        getDaoRoom();

        insertBaseData();
    }

    public void insertBaseData(){
        //init admin
        try {
            daoUser.createIfNotExists(new User("Admin","Admin","admin","123",true, "leo.lemarie@gmail.com", "0761115152"));
            Log.i("insert", "insert admins");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //init Games
        try {
            daoGame.createIfNotExists(new Game(Game.SIGN));
            daoGame.createIfNotExists(new Game(Game.IMAGE));
            Log.i("insert", "insert games");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    //GS
    public Dao<Game, String> getDaoGame(){
        if (daoGame == null) {
            try {
                daoGame = DaoManager.createDao(connectionSource, Game.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return daoGame;
    }

    public Dao<Room, String> getDaoRoom(){
        if (daoRoom == null) {
            try {
                daoRoom = DaoManager.createDao(connectionSource, Room.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return daoRoom;
    }

    public Dao<User, String> getDaoUser(){
        if (daoUser == null) {
            try {
                daoUser = DaoManager.createDao(connectionSource, User.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return daoUser;
    }

    public Dao<Person, String> getDaoPerson(){
        if (daoPerson == null) {
            try {
                daoPerson = DaoManager.createDao(connectionSource, Person.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return daoPerson;
    }

    public Dao<Stage, String> getDaoStage(){
        if (daoStage == null) {
            try {
                daoStage = DaoManager.createDao(connectionSource, Stage.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return daoStage;
    }

    public User getCurrentUser(){
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {}


}
