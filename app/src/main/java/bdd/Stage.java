package bdd;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

/**
 * Created by leuks on 03/07/2016.
 */
@DatabaseTable(tableName = "stage")
public class Stage implements Parcelable, Comparable<Date>{

    @DatabaseField(id = true)
    private Integer id;

    @DatabaseField
    private String reason;

    @DatabaseField
    private String comment;

    @DatabaseField(foreign = true, foreignAutoRefresh=true)
    private User user;

    @DatabaseField(foreign = true, foreignAutoRefresh=true)
    private Person person;

    @DatabaseField
    private Date creationDate;

    @ForeignCollectionField(eager = true)
    private Collection<Room> rooms;


    public Stage(){
        this.rooms = new ArrayList<Room>();
        this.creationDate = Calendar.getInstance().getTime();
    }

    public Stage(DBManager dbManager){
        this.id = dbManager.getLastStageId();
        this.rooms = new ArrayList<Room>();
        this.creationDate = Calendar.getInstance().getTime();
    }


    //parcelable
    public Stage(Parcel in){
        this();
        this.id = in.readInt();
        this.comment = in.readString();
        this.reason = in.readString();
        this.user = in.readParcelable(User.class.getClassLoader());
        this.person = in.readParcelable(Person.class.getClassLoader());
        in.readTypedList(new ArrayList<Room>(rooms), Room.CREATOR);
        for(Room room: rooms) room.setStage(this);
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(comment);
        dest.writeString(reason);
        dest.writeParcelable(user, 0);
        dest.writeParcelable(person, 0);
        dest.writeTypedList(new ArrayList<Room>(rooms));
        dest.writeLong(creationDate.getTime());
    }

    public static final Parcelable.Creator<Stage> CREATOR =
            new Parcelable.Creator<Stage>() {
                public Stage createFromParcel(Parcel in) {
                    return new Stage(in);
                }

                public Stage[] newArray(int size) {
                    return new Stage[size];
                }
            };

    public boolean isCreated(){
        return ((user!=null) && (person!=null) && (comment!=null) && (reason!=null));
    }

    //GS
    public Collection<Room> getRooms(){
        return rooms;
    }

    public User getUser(){
        return user;
    }

    public Person getPerson(){
        return person;
    }

    public String getReason(){
        return reason;
    }

    public String getComment(){
        return comment;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Date getCreationDate(){
        return this.creationDate;
    }
    public String getToStringCreation(){
        return DateFormat.getDateInstance(DateFormat.LONG, Locale.FRANCE).format(creationDate);
    }

    public Integer getId() {
        return id;
    }

    @Override
    public int compareTo(Date another) {
        if(creationDate.getDay() == another.getDay() && creationDate.getMonth() == another.getMonth() && creationDate.getYear() == another.getYear()) return 0;
        else return creationDate.compareTo(another);
    }
}
