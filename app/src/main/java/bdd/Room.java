package bdd;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by leuks on 04/07/2016.
 */
@DatabaseTable(tableName = "room")
public class Room implements Parcelable{

    @DatabaseField(id = true)
    private Integer id;

    @DatabaseField
    private String score;

    @DatabaseField
    private boolean userValidation;

    @DatabaseField(foreign = true)
    private Stage stage;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private Game game;

    private Object tamps;

    public Room(){
    }

    public Room(Game game, Stage stage, DBManager dbManager, int count){
        this.game = game;
        this.stage = stage;
        this.id = dbManager.getLastRoomId()+count;
        Log.i("create room",""+id);
    }


    //parcelable
    public Room(Parcel in){
        this.id = in.readInt();
        this.game = in.readParcelable(Game.class.getClassLoader());
        this.score = in.readString();
        this.userValidation = in.readByte() != 0;
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeParcelable(game, 0);
        dest.writeString(score);
        dest.writeByte((byte) (userValidation ? 1 : 0));
    }

    public static final Parcelable.Creator<Room> CREATOR =
            new Parcelable.Creator<Room>() {
                public Room createFromParcel(Parcel in) {
                    return new Room(in);
                }

                public Room[] newArray(int size) {
                    return new Room[size];
                }
            };


    //GS
    public Game getGame() {
        return game;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public void setUserValidation(boolean userValidation){
        this.userValidation = userValidation;
    }

    public String getScore() {
        return score;
    }

    public boolean isUserValidation() {
        return userValidation;
    }

    public Integer getId() {
        return id;
    }

    public void setTamps(Object tamps) {
        this.tamps = tamps;
    }

    public Object getTamps() {
        return tamps;
    }
}