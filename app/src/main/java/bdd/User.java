package bdd;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by leuks on 03/07/2016.
 */
@DatabaseTable(tableName = "user")
public class User implements Parcelable{
    @DatabaseField(generatedId = true)
    private Integer id;

    @DatabaseField
    private String name;

    @DatabaseField
    private String firstname;

    @DatabaseField(unique = true)
    private String login;

    @DatabaseField
    private String password;

    @DatabaseField
    private boolean admin;

    @DatabaseField
    private String email;

    @DatabaseField
    private String phone;

    @DatabaseField
    private Date creationDate;

    public User(){}

    public User(String name, String firstname, String login, String password, boolean admin, String email, String phone){
        this.name = name;
        this.firstname = firstname;
        this.login = login;
        this.password = password;
        this.admin = admin;
        this.email = email;
        this.phone = phone;
        this.creationDate = Calendar.getInstance().getTime();
    }

    //parcelable

    public User(Parcel in){
        this.id = in.readInt();
        this.name = in.readString();
        this.firstname = in.readString();
        this.login = in.readString();
        this.password = in.readString();
        this.admin = in.readByte() != 0;
        this.email = in.readString();
        this.phone = in.readString();
        this.creationDate = new Date(in.readLong());
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(firstname);
        dest.writeString(login);
        dest.writeString(password);
        dest.writeByte((byte) (admin ? 1 : 0));
        dest.writeString(email);
        dest.writeString(phone);
        dest.writeLong(creationDate.getTime());
    }

    public static final Parcelable.Creator<User> CREATOR =
            new Parcelable.Creator<User>() {
                public User createFromParcel(Parcel in) {
                    return new User(in);
                }

                public User[] newArray(int size) {
                    return new User[size];
                }
            };

    //GS
    public String getName(){
        return name;
    }

    public String getFirstname(){
        return firstname;
    }

    public String getLogin(){
        return login;
    }

    public String getPassword(){
        return password;
    }

    public boolean getAdmin(){
        return admin;
    }

    public String getEmail(){return email;}

    public String getPhone(){return phone;}

    public String getToStringCreation(){
        return DateFormat.getDateInstance(DateFormat.LONG, Locale.FRANCE).format(creationDate);
    }
}
