package bdd;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by leuks on 03/07/2016.
 */
@DatabaseTable(tableName = "game")
public class Game implements Parcelable {
    @DatabaseField(generatedId = true)
    private Integer id;

    @DatabaseField(unique = true)
    private String name;

    @DatabaseField
    private String parameters;

    public static final String SIGN = "Temps de réaction";
    public static final String IMAGE = "Capacité attentionnelle";

    public Game(){}

    public Game(String name){
        this.name = name;
        bindParameters();
    }

    private void bindParameters(){
        switch(name){
            case SIGN:
                this.parameters = "3.0/50/20/1000";
                break;
            case IMAGE:
                this.parameters = "1000/50";
                break;
        }
    }

    //parcelable
    public Game(Parcel in){
        this.id = in.readInt();
        this.name = in.readString();
        this.parameters = in.readString();
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(parameters);
    }

    public static final Parcelable.Creator<Game> CREATOR =
            new Parcelable.Creator<Game>() {
                public Game createFromParcel(Parcel in) {
                    return new Game(in);
                }

                public Game[] newArray(int size) {
                    return new Game[size];
                }
            };


    //GS
    public String getName(){
        return name;
    }

    public String getParameters(){
        return parameters;
    }

    public void setParameters(String parameters){
        this.parameters = parameters;
    }

}