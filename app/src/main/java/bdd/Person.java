package bdd;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by leuks on 03/07/2016.
 */
@DatabaseTable(tableName = "person")
public class Person implements Parcelable{
    @DatabaseField(generatedId = true)
    private Integer id;

    @DatabaseField
    private String name;

    @DatabaseField
    private String firstname;

    @DatabaseField
    private boolean gender;

    @DatabaseField(unique = true)
    private String email;

    @DatabaseField
    private Date birth;

    @DatabaseField
    private String phone;

    @DatabaseField
    private Date creationDate;

    @DatabaseField
    private String departement;

    @DatabaseField
    private boolean cause;

    public static final boolean CAUSE_SUSPENTION = false;
    public static final boolean CAUSE_RETRAIT = true;

    public Person(){}

    public Person(String name, String firstname, boolean gender, Date birth, String email, String phone, String departement, boolean cause){
        this.name = name;
        this.firstname = firstname;
        this.gender = gender;
        this.birth = birth;
        this.email = email;
        this.phone = phone;
        this.departement = departement;
        this.cause = cause;
        this.creationDate = Calendar.getInstance().getTime();
    }

    //parcelable
    public Person(Parcel in){
        this.id = in.readInt();
        this.name = in.readString();
        this.firstname = in.readString();
        this.gender = in.readByte() != 0;
        this.birth = new Date(in.readLong());
        this.email = in.readString();
        this.phone = in.readString();
        this.departement = in.readString();
        this.cause = in.readByte() != 0;
        this.creationDate = new Date(in.readLong());
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(firstname);
        dest.writeByte((byte) (gender ? 1 : 0));
        dest.writeLong(creationDate.getTime());
        dest.writeString(email);
        dest.writeString(phone);
        dest.writeString(departement);
        dest.writeByte((byte) (cause ? 1 : 0));
        dest.writeLong(creationDate.getTime());

    }

    public static final Parcelable.Creator<Person> CREATOR =
            new Parcelable.Creator<Person>() {
                public Person createFromParcel(Parcel in) {
                    return new Person(in);
                }

                public Person[] newArray(int size) {
                    return new Person[size];
                }
            };

    public String getName(){
        return name;
    }

    public String getFirstname(){
        return firstname;
    }

    public boolean getGender(){
        return gender;
    }

    public Date getBirth(){ return birth; }

    public String getEmail(){
        return email;
    }

    public String getPhone(){
        return phone;
    }

    public Date getCreationDate(){ return creationDate; }

    public String getDepartement(){ return departement; }

    public String getToStringBirth(){
        return DateFormat.getDateInstance(DateFormat.LONG, Locale.FRANCE).format(birth);
    }
}
