package com.app.aacc.app_manager.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;

import com.app.aacc.R;
import com.app.aacc.app_manager.AppManager;

import java.util.HashMap;

import Entities.Tools;
import bdd.DBManager;
import bdd.Game;

/**
 * Created by leuks on 16/07/2016.
 */
public class ParamFragment extends Fragment {
    private String game;
    private AppManager appManager;
    private DBManager dbManager;

    private SeekBar scoreBar;
    private SeekBar timeBar;
    private EditText timeValueBar;
    private EditText scoreValueBar;
    private SeekBar scoreBadBar;
    private SeekBar timePanBar;
    private EditText scoreBadValueBar;
    private EditText timePanValueBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.appManager = (AppManager) getActivity();
        dbManager = appManager.getHelper();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View thisView = null;
        switch (game){
            case Game.IMAGE:
                thisView = inflater.inflate(R.layout.parameters_image_game, container, false);
                timeBar = (SeekBar) thisView.findViewById(R.id.param_time_image_game);
                scoreBar = (SeekBar) thisView.findViewById(R.id.param_score_image_game);
                timeValueBar = (EditText) thisView.findViewById(R.id.param_time_value_image_game);
                scoreValueBar = (EditText) thisView.findViewById(R.id.param_score_value_image_game);
                Button submitButton = (Button) thisView.findViewById(R.id.submit_params_image_game);


                timeBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        timeValueBar.setText(String.valueOf(progress));
                    }
                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {}
                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {}
                });

                timeValueBar.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        timeValueBar.setSelection(timeValueBar.getText().length());
                        Tools.changeStatusEdit(false, timeValueBar, appManager);
                        String stringValue = timeValueBar.getText().toString().trim();
                        if(!stringValue.isEmpty()) {
                            int value = Integer.parseInt(stringValue);
                            if (value >= 1 && value <= 10000) {
                                timeBar.setProgress(value);
                            } else {
                                Tools.changeStatusEdit(true, timeValueBar, appManager);
                            }
                        }
                        else{
                            Tools.changeStatusEdit(true, timeValueBar, appManager);
                            Tools.textOut(appManager, "Le temps ne peut être null");
                        }
                    }
                });



                scoreBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        scoreValueBar.setText(String.valueOf(progress));
                    }
                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {}
                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {}
                });

                scoreValueBar.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        Tools.changeStatusEdit(false, scoreValueBar, appManager);
                    }
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}

                    @Override
                    public void afterTextChanged(Editable s) {
                        scoreValueBar.setSelection(scoreValueBar.getText().length());
                        Tools.changeStatusEdit(false, scoreValueBar, appManager);
                        String stringValue = scoreValueBar.getText().toString().trim();
                        if(!stringValue.isEmpty()) {
                            int value = Integer.parseInt(stringValue);
                            if (value >= 1 && value <= 100) {
                                scoreBar.setProgress(value);
                            } else {
                                Tools.changeStatusEdit(true, scoreValueBar, appManager);
                            }
                        }
                        else{
                            Tools.changeStatusEdit(true, scoreValueBar, appManager);
                            Tools.textOut(appManager, "Le score ne peut être null");
                        }
                    }
                });

                //button submit
                submitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Tools.changeStatusEdit(false, timeValueBar, appManager);
                        Tools.changeStatusEdit(false, scoreValueBar, appManager);

                        String stringTime = timeValueBar.getText().toString().trim();
                        String stringScore = scoreValueBar.getText().toString().trim();

                        if(stringTime.isEmpty()) Tools.changeStatusEdit(true, timeValueBar, appManager);
                        else if(stringScore.isEmpty()) Tools.changeStatusEdit(true, scoreValueBar, appManager);
                        else{
                            int time = Integer.parseInt(stringTime);
                            int score = Integer.parseInt(stringScore);

                            if(time < 1 || time > 10000){
                                Tools.changeStatusEdit(true, timeValueBar, appManager);
                                Tools.textOut(appManager, "Temps entre 1 et 10000 ms");

                            }
                            else if(score<1 || score >100){
                                Tools.changeStatusEdit(true, scoreValueBar, appManager);
                                Tools.textOut(appManager, "Score entre 1 et 100 %");
                            }
                            else {
                                String newParams = time + "/" + score;
                                Tools.textOut(appManager, "Paramètres actualisés");
                                dbManager.setParameters(Game.IMAGE, newParams);
                                appManager.getFragmentManager().popBackStackImmediate();
                            }
                        }
                    }
                });

                break;


            case Game.SIGN:
                thisView = inflater.inflate(R.layout.parameters_sign_game, container, false);
                timeBar = (SeekBar) thisView.findViewById(R.id.param_time_sign_game);
                scoreBar = (SeekBar) thisView.findViewById(R.id.param_good_sign_game);
                timeValueBar = (EditText) thisView.findViewById(R.id.param_time_value_sign_game);
                scoreValueBar = (EditText) thisView.findViewById(R.id.param_good_value_sign_game);
                scoreBadBar = (SeekBar) thisView.findViewById(R.id.param_bad_sign_game);
                timePanBar = (SeekBar) thisView.findViewById(R.id.param_time_pan_sign_game);
                scoreBadValueBar = (EditText) thisView.findViewById(R.id.param_bad_value_sign_game);
                timePanValueBar = (EditText) thisView.findViewById(R.id.param_time_pan__value_sign_game);

                submitButton = (Button) thisView.findViewById(R.id.submit_params_image_game);


                timeBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        timeValueBar.setText(String.valueOf(progress));
                    }
                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {}
                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {}
                });

                timeValueBar.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        timeValueBar.setSelection(timeValueBar.getText().length());
                        Tools.changeStatusEdit(false, timeValueBar, appManager);
                        String stringValue = timeValueBar.getText().toString().trim();
                        if(!stringValue.isEmpty()) {
                            double value = Double.parseDouble(stringValue);
                            if (value >= 1 && value <= 10) {
                                timeBar.setProgress((int)value);
                            } else {
                                Tools.changeStatusEdit(true, timeValueBar, appManager);
                            }
                        }
                        else{
                            Tools.changeStatusEdit(true, timeValueBar, appManager);
                            Tools.textOut(appManager, "Le temps ne peut être null");
                        }
                    }
                });



                scoreBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        scoreValueBar.setText(String.valueOf(progress));
                    }
                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {}
                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {}
                });

                scoreValueBar.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        Tools.changeStatusEdit(false, scoreValueBar, appManager);
                    }
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}

                    @Override
                    public void afterTextChanged(Editable s) {
                        scoreValueBar.setSelection(scoreValueBar.getText().length());
                        Tools.changeStatusEdit(false, scoreValueBar, appManager);
                        String stringValue = scoreValueBar.getText().toString().trim();
                        if(!stringValue.isEmpty()) {
                            int value = Integer.parseInt(stringValue);
                            if (value >= 1 && value <= 100) {
                                scoreBar.setProgress(value);
                            } else {
                                Tools.changeStatusEdit(true, scoreValueBar, appManager);
                            }
                        }
                        else{
                            Tools.changeStatusEdit(true, scoreValueBar, appManager);
                            Tools.textOut(appManager, "Le score minimum ne peut être null");
                        }
                    }
                });


                scoreBadBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        scoreBadValueBar.setText(String.valueOf(progress));
                    }
                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {}
                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {}
                });

                scoreBadValueBar.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        scoreBadValueBar.setSelection(scoreBadValueBar.getText().length());
                        Tools.changeStatusEdit(false, scoreBadValueBar, appManager);
                        String stringValue = scoreBadValueBar.getText().toString().trim();
                        if(!stringValue.isEmpty()) {
                            int value = Integer.parseInt(stringValue);
                            if (value >= 1 && value <= 100) {
                                scoreBadBar.setProgress(value);
                            } else {
                                Tools.changeStatusEdit(true, scoreBadValueBar, appManager);
                            }
                        }
                        else{
                            Tools.changeStatusEdit(true, scoreBadValueBar, appManager);
                            Tools.textOut(appManager, "Le score minimum ne peut être null");
                        }
                    }
                });



                timePanBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        timePanValueBar.setText(String.valueOf(progress));
                    }
                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {}
                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {}
                });

                timePanValueBar.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}

                    @Override
                    public void afterTextChanged(Editable s) {
                        timePanValueBar.setSelection(timePanValueBar.getText().length());
                        Tools.changeStatusEdit(false, timePanValueBar, appManager);
                        String stringValue = timePanValueBar.getText().toString().trim();
                        if(!stringValue.isEmpty()) {
                            int value = Integer.parseInt(stringValue);
                            if (value >= 1 && value <= 2000) {
                                timePanBar.setProgress(value);
                            } else {
                                Tools.changeStatusEdit(true, timePanValueBar, appManager);
                            }
                        }
                        else{
                            Tools.changeStatusEdit(true, timePanValueBar, appManager);
                            Tools.textOut(appManager, "Le temps d'affichage ne peut être null");
                        }
                    }
                });

                //button submit
                submitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                                Tools.changeStatusEdit(false, timeValueBar, appManager);
                                Tools.changeStatusEdit(false, scoreValueBar, appManager);
                                Tools.changeStatusEdit(false, scoreValueBar, appManager);
                                Tools.changeStatusEdit(false, timePanValueBar, appManager);

                                String stringTimeGame = timeValueBar.getText().toString().trim();
                                String stringScoreGood = scoreValueBar.getText().toString().trim();
                                String stringScoreBad = scoreBadValueBar.getText().toString().trim();
                                String stringTimePan = timePanValueBar.getText().toString().trim();

                                if(stringTimeGame.isEmpty()) Tools.changeStatusEdit(true, timeValueBar, appManager);
                                else if(stringScoreGood.isEmpty()) Tools.changeStatusEdit(true, scoreValueBar, appManager);
                                else if(stringScoreBad.isEmpty()) Tools.changeStatusEdit(true, scoreBadValueBar, appManager);
                                else if(stringTimePan.isEmpty()) Tools.changeStatusEdit(true, timePanValueBar, appManager);
                                else{
                                    double timeGame = Double.parseDouble(stringTimeGame);
                                    int scoreGood = Integer.parseInt(stringScoreGood);
                                    int scoreBad = Integer.parseInt(stringScoreBad);
                                    int timePan = Integer.parseInt(stringTimePan);

                                    if(timeGame < 1 || timeGame > 10){
                                        Tools.changeStatusEdit(true, timeValueBar, appManager);
                                        Tools.textOut(appManager, "Temps entre 1 et 10 minutes");

                                    }
                                    else if(scoreGood<1 || scoreGood >100){
                                        Tools.changeStatusEdit(true, scoreValueBar, appManager);
                                        Tools.textOut(appManager, "Score entre 1 et 100 %");
                                    }
                                    else if(scoreBad<1 || scoreBad >100){
                                        Tools.changeStatusEdit(true, scoreBadValueBar, appManager);
                                        Tools.textOut(appManager, "Score entre 1 et 100 %");
                                    }
                                    else if(timePan<1 || timePan >2000){
                                        Tools.changeStatusEdit(true, timePanValueBar, appManager);
                                        Tools.textOut(appManager, "Temps d'affichage entre 1 et 2000 ms");
                                    }
                                    else {
                                        String newParams = timeGame + "/" + scoreGood + "/" + scoreBad + "/" + timePan;
                                        Tools.textOut(appManager, "Paramètres actualisés");
                                        dbManager.setParameters(Game.SIGN, newParams);
                                        appManager.getFragmentManager().popBackStackImmediate();
                                    }
                                }
                    }
                });
                break;
        }

        return thisView;
    }

    @Override
    public void onResume() {
        super.onResume();

        switch(game){
            case Game.IMAGE:
                HashMap<String, String> params = dbManager.getParameters(Game.IMAGE);

                //time init
                int time = Integer.parseInt(params.get("time"));
                timeBar.setProgress(time);
                timeValueBar.setText(String.valueOf(time));

                //score init
                int score = Integer.parseInt(params.get("score"));
                scoreBar.setProgress(score);
                scoreValueBar.setText(String.valueOf(score));
                break;
            case Game.SIGN:
                HashMap<String, String> paramsSign = dbManager.getParameters(Game.SIGN);

                //time init
                double timeGame = Double.parseDouble(paramsSign.get("total_time"));
                timeBar.setProgress((int)timeGame);
                timeValueBar.setText(String.valueOf(timeGame));

                //score good init
                int scoreGood = Integer.parseInt(paramsSign.get("score_good"));
                scoreBar.setProgress(scoreGood);
                scoreValueBar.setText(String.valueOf(scoreGood));

                //score bad init
                int scoreBad = Integer.parseInt(paramsSign.get("score_bad"));
                scoreBadBar.setProgress(scoreBad);
                scoreBadValueBar.setText(String.valueOf(scoreBad));

                //time pan init
                int timePan = Integer.parseInt(paramsSign.get("display_time"));
                timePanBar.setProgress(timePan);
                timePanValueBar.setText(String.valueOf(timePan));

                break;
        }

    }

    public void init(String game){
        this.game = game;
    }
}
