package com.app.aacc.app_manager.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.aacc.app_manager.AppManager;
import com.app.aacc.R;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Entities.Tools;
import bdd.DBManager;
import bdd.Game;

/**
 * Created by leuks on 04/07/2016.
 */
public class ChooseGameFragment extends Fragment {
    private List<Game> gameList;
    private ListView gameListView;
    private int counter;
    private AppManager appManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appManager = (AppManager) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.choose_game_fragment_app_manager, container, false);
        this.gameListView = (ListView) thisView.findViewById(R.id.game_list_view);
        final Button buttonSubmitGameChoice = (Button) thisView.findViewById(R.id.submit_game_choice);
        final AppManager appManager = (AppManager) getActivity();
        final DBManager dbManager = appManager.getHelper();
        this.counter=1;

        try {
            gameList = dbManager.getDaoGame().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //set adapter
        gameListView.setAdapter(new ListAdapter(appManager, gameList));

        //set listView listener
        gameListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView gameNumberField = (TextView) view.findViewById(R.id.game_choose_number);
                boolean isNumberEmpty = gameNumberField.getText().toString().trim().isEmpty();

                if(isNumberEmpty) {
                    gameNumberField.setText(String.valueOf(counter));
                    counter++;
                }
                else {
                    gameNumberField.setText("");


                    for (int i = 0; i < gameListView.getCount(); i++) {
                        View currentView = gameListView.getChildAt(i);
                        TextView actualGameNumberField = (TextView) currentView.findViewById(R.id.game_choose_number);
                        String actualChildValue = actualGameNumberField.getText().toString().trim();
                        boolean isChildNumberEmpty = actualChildValue.isEmpty();
                        if (!isChildNumberEmpty) {
                            int actualCount = Integer.valueOf(actualChildValue);
                            if (actualCount - 1 != 0) actualGameNumberField.setText(String.valueOf(actualCount - 1));
                        }
                    }
                    counter--;
                }
            }
        });

        //button submit
        buttonSubmitGameChoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Game> orderGameArray = new ArrayList();
                int turnGameValue = 1;
                for(int a=0; a<gameListView.getCount(); a++) {
                    for(int i=0; i<gameListView.getCount(); i++) {
                        View currentView = gameListView.getChildAt(i);
                        TextView actualGameNumberField = (TextView) currentView.findViewById(R.id.game_choose_number);
                        String actualChildValue = actualGameNumberField.getText().toString().trim();
                        if(!actualChildValue.isEmpty()) {
                            int actualIntChildValue = Integer.parseInt(actualChildValue);
                            if (actualIntChildValue == turnGameValue) {
                                orderGameArray.add(gameList.get(i));
                                turnGameValue++;
                            }
                        }
                    }
                }
                if(orderGameArray.size() == 0) Tools.textOut(appManager, "Aucun test séléctionné");
                else appManager.createRooms(orderGameArray);
            }
        });

        return thisView;
    }


    // adapter
    private class ListAdapter extends ArrayAdapter {
        private Context context;
        private List<Game> data;

        public ListAdapter(Context context, List<Game> data){
            super(context, -1, data);
            this.context = context;
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.game_item_app_manager, parent, false);
            Game game = data.get(position);
            TextView gameTitle = (TextView) row.findViewById(R.id.game_title);

            gameTitle.setText(game.getName());

            return row;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        counter = 1;
        for (int i = 0; i < gameListView.getCount(); i++) {
            View currentView = gameListView.getChildAt(i);
            if(currentView != null) {
                TextView gameNumberField = (TextView) currentView.findViewById(R.id.game_choose_number);
                gameNumberField.setText("");
            }
        }
    }
}
