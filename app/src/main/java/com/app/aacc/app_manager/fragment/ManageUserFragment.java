package com.app.aacc.app_manager.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.app.aacc.app_manager.AppManager;
import com.app.aacc.R;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.wdullaer.swipeactionadapter.SwipeActionAdapter;
import com.wdullaer.swipeactionadapter.SwipeDirection;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Entities.Tools;
import bdd.DBManager;
import bdd.Person;
import bdd.User;

/**
 * Created by leuks on 07/07/2016.
 */
public class ManageUserFragment extends Fragment {
    private ArrayList<User> userList;
    private List<User> originalUserList;
    private SwipeActionAdapter SWAdapter;
    private SwipeMenuListView userListView;
    private AppManager appManager;
    private ListAdapter listAdapter;
    private EditText searchField;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.appManager = (AppManager) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.manage_user_fragment_app_manager, container, false);
        final Button addButton = (Button) thisView.findViewById(R.id.moreUser);
        searchField = (EditText) thisView.findViewById(R.id.searchUser);
        final DBManager dbManager = appManager.getHelper();
        this.userListView = (SwipeMenuListView) thisView.findViewById(R.id.userList);

        // adapter & listeners
        try {
            originalUserList = dbManager.getDaoUser().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        this.userList = new ArrayList(originalUserList);

        //ListMenuCreator
        SwipeMenuCreator swipeMenuCreator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu swipeMenu) {
                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        appManager.getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(90);
                // set a icon
                deleteItem.setIcon(R.drawable.remove);
                // add to menu
                swipeMenu.addMenuItem(deleteItem);
            }
        };
        userListView.setMenuCreator(swipeMenuCreator);

        //click menu list listener
        userListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {

                switch (index) {
                    case 0:
                            final User userToDelete = userList.get(position);
                            if(userToDelete.getLogin().equals(appManager.getCurrentUser().getLogin())){
                                Tools.textOut(appManager, "Impossible de supprimer le compte courant");
                            }
                            else{
                                AlertDialog dialog = null;
                                AlertDialog.Builder builder = new AlertDialog.Builder(appManager);
                                builder.setMessage("Supprimer l'utilisateur?")
                                        .setTitle("Suppresion");
                                builder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dbManager.deleteUser(userList.get(position));
                                        userList.remove(position);
                                        notice();
                                        Tools.textOut(appManager, "Utilisateur "+userToDelete.getFirstname()+" - "+userToDelete.getName()+" supprimé");
                                    }
                                });
                                builder.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                                dialog = builder.create();
                                dialog.show();
                            }

                        break;
                }

                return false; //false/true not close
            }
        });

        //swape authorisation
        userListView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);

        //adapter
        listAdapter = new ListAdapter(appManager, userList);
        userListView.setAdapter(listAdapter);

        // button add
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appManager.show(AppManager.ROAD_NEW_USER, true);
            }
        });

        //search field
        searchField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                userList.clear();
                String textSearch = searchField.getText().toString();
                for (User user : originalUserList) {
                    if (user.getFirstname().toLowerCase().contains(textSearch.toLowerCase()) || user.getName().toLowerCase().contains(textSearch.toLowerCase())) {
                        userList.add(user);
                    }
                }
                notice();
            }
        });
        return thisView;
    }

    @Override
    public void onResume() {
        super.onResume();
        searchField.getText().clear();
    }

    // adapter
    private class ListAdapter extends ArrayAdapter {
        private Context context;
        private List<User> data;

        public ListAdapter(Context context, List<User> data){
            super(context, -1, data);
            this.context = context;
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.user_item_app_manager, parent, false);
            User user = data.get(position);
            TextView firstname = (TextView) row.findViewById(R.id.firstname_item_user);
            TextView name = (TextView) row.findViewById(R.id.name_item_user);

            firstname.setText(user.getFirstname());
            name.setText(user.getName());

            if(user.getAdmin()){
                Tools.changeSpecialView(true, row, appManager);
            }

            return row;
        }
    }

    private void notice(){
        listAdapter.notifyDataSetChanged();
        userListView.setAdapter(listAdapter);
    }
}
