package com.app.aacc.app_manager.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.aacc.R;
import com.app.aacc.app_manager.AppManager;

/**
 * Created by leuks on 19/09/2016.
 */
public class ManualFragment extends Fragment {
    private AppManager appManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appManager = (AppManager) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.manual_fragment, container, false);

        return thisView;
    }
}
