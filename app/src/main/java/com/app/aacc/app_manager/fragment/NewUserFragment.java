package com.app.aacc.app_manager.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;

import com.app.aacc.app_manager.AppManager;
import com.app.aacc.R;

import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import Entities.Tools;
import bdd.Person;
import bdd.User;

/**
 * Created by leuks on 07/07/2016.
 */
public class NewUserFragment extends Fragment {
    private EditText firstnameField;
    private EditText nameField;
    private EditText emailField;
    private EditText loginField;
    private EditText passwordField;
    private EditText phoneField;
    private Switch adminSwitch;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.create_user_fragment_app_manager, container, false);
        final Button buttonNewSubmit = (Button) thisView.findViewById(R.id.new_user_submit);
        final AppManager appManager = (AppManager) getActivity();

        this.firstnameField = (EditText) thisView.findViewById(R.id.new_user_firstname);
        this.nameField = (EditText) thisView.findViewById(R.id.new_user_name);
        this.emailField = (EditText) thisView.findViewById(R.id.new_user_email);
        this.loginField = (EditText) thisView.findViewById(R.id.new_user_login);
        this.passwordField = (EditText) thisView.findViewById(R.id.new_user_password);
        this.phoneField = (EditText) thisView.findViewById(R.id.new_user_phone);
        this.adminSwitch = (Switch) thisView.findViewById(R.id.new_user_admin);

        //button submit
        buttonNewSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String firstname = firstnameField.getText().toString().trim();
                String name = nameField.getText().toString().trim();
                String email = emailField.getText().toString().trim();
                String login = loginField.getText().toString().trim();
                String password = passwordField.getText().toString().trim();
                String phone = phoneField.getText().toString().trim();
                boolean isAdmin = adminSwitch.isChecked();


                Tools.changeStatusEdit(false, firstnameField, appManager);
                Tools.changeStatusEdit(false, nameField, appManager);
                Tools.changeStatusEdit(false, loginField, appManager);
                Tools.changeStatusEdit(false, passwordField, appManager);
                Tools.changeStatusEdit(false, emailField, appManager);
                Tools.changeStatusEdit(false, phoneField, appManager);

                //check empty
                if(firstname.isEmpty()){
                    Tools.changeStatusEdit(true, firstnameField, appManager);
                    Tools.textOut(appManager, "Le prénom ne doit pas être vide");
                }
                else if(name.isEmpty()){
                    Tools.changeStatusEdit(true, nameField, appManager);
                    Tools.textOut(appManager, "Le nom ne doit pas être vide");
                }
                else if(login.isEmpty()){
                    Tools.changeStatusEdit(true, loginField, appManager);
                    Tools.textOut(appManager, "Le login ne doit pas être vide");
                }
                else if(password.isEmpty()){
                    Tools.changeStatusEdit(true, passwordField, appManager);
                    Tools.textOut(appManager, "Le mot de passe ne doit pas être vide");
                }
                else if(email.isEmpty()){
                    Tools.changeStatusEdit(true, emailField, appManager);
                    Tools.textOut(appManager, "L'email ne doit pas être vide");
                }
                else if(!phone.isEmpty()){
                    if(!Pattern.compile("\\d{10}").matcher(phone).matches()){
                        Tools.changeStatusEdit(true, phoneField, appManager);
                        Tools.textOut(appManager, "Le numéro doit contenir 10 chiffres");
                    }
                }
                //check regex
                else if(!Pattern.compile(".+@+.*").matcher(email).matches()) {
                    Tools.changeStatusEdit(true, emailField, appManager);
                    Tools.textOut(appManager, "L'email est incorrect");
                }
                else{
                    User user = new User(firstname, name, login, password, isAdmin, email, phone);
                    int result = appManager.getHelper().createUser(user);
                    switch (result) {
                        case -1:
                            Tools.textOut(appManager, "Problème lors de l'ajout");
                        case 0:
                            Tools.textOut(appManager, "Compte créé");
                            appManager.getFragmentManager().popBackStackImmediate();
                            appManager.show(AppManager.ROAD_MANAGE_ACCOUNT, false);
                            break;
                        case 1:
                            Tools.changeStatusEdit(true, loginField, appManager);
                            break;
                    }
                }
            }
        });

        return thisView;
    }

    @Override
    public void onResume() {
        super.onResume();
        //clear field
        firstnameField.getText().clear();
        nameField.getText().clear();
        emailField.getText().clear();
        loginField.getText().clear();
        passwordField.getText().clear();
        phoneField.getText().clear();
        adminSwitch.setChecked(false);
    }

}
