package com.app.aacc.app_manager.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.Button;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Created by leuks on 04/07/2016.
 */
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    private Calendar calendar;
    private DatePickerDialog datePickerDialog;
    private Button buttonNewBirth;

    public DatePickerFragment(){
        this.calendar = Calendar.getInstance();
     }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        this.datePickerDialog = new DatePickerDialog(getActivity(), this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        return datePickerDialog;
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        calendar.set(year,month,day);
        buttonNewBirth.setText(toString());
    }

    @Override
    public String toString(){
        return calendar.get(Calendar.DAY_OF_MONTH) + " / " + (calendar.get(Calendar.MONTH)+1) + " / " + calendar.get(Calendar.YEAR);
    }

    //GS
    public Calendar getCalendar(){
        return calendar;
    }

    public void setHandled(Button button){
        this.buttonNewBirth = button;
        buttonNewBirth.setText(toString());
    }

}