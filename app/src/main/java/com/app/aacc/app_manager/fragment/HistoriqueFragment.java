package com.app.aacc.app_manager.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.text.Editable;
import android.text.Layout;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.app.aacc.R;
import com.app.aacc.app_manager.AppManager;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import Entities.EmailTask;
import Entities.Tools;
import bdd.DBManager;
import bdd.Room;
import bdd.Stage;
import jxl.biff.drawing.ComboBox;

/**
 * Created by leuks on 17/07/2016.
 */
public class HistoriqueFragment extends Fragment {
    private AppManager appManager;
    private ListView stageListView;
    private List<Stage> stageList;
    private DBManager dbManager;

    private Spinner spinnerChoix;
    private LinearLayout searchBar_stage;
    private DatePickerFragment datePickerFragment;
    private boolean checkable;
    private LinearLayout linearLayoutSelection;
    private CheckBox checkAll;
    private ArrayList<String> selectedItems;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.appManager = (AppManager) getActivity();
        dbManager = appManager.getHelper();
        datePickerFragment = new DatePickerFragment();
        stageList = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.historique_app_manager, container, false);
        this.stageListView = (ListView) thisView.findViewById(R.id.stageList);
        this.spinnerChoix = (Spinner) thisView.findViewById(R.id.spinner_date_histo);
        this.searchBar_stage = (LinearLayout) thisView.findViewById(R.id.searchBar_stage);
        final Button butExp = (Button) thisView.findViewById(R.id.but_exp);
        final Button buttonImp = (Button) thisView.findViewById(R.id.button_imp);
        checkable = false;
        selectedItems = new ArrayList();

        //selection count
        linearLayoutSelection = new LinearLayout(appManager);
        checkAll = new CheckBox(appManager);
        checkAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    for(int i=0; i<stageListView.getCount(); i++  ) {
                        View rowView = stageListView.getChildAt(i);
                        if(rowView != null)((CheckBox) rowView.findViewById(R.id.checkbox_id)).setChecked(true);
                        if(!selectedItems.contains(String.valueOf(i))) selectedItems.add(String.valueOf(i));
                    }
                }
                else{
                    for(int i=0; i<stageListView.getCount(); i++  ) {
                        View rowView = stageListView.getChildAt(i);
                        if(rowView != null) ((CheckBox) rowView.findViewById(R.id.checkbox_id)).setChecked(false);
                        selectedItems.clear();
                    }
                }
            }
        });
        TextView tousTextView = new TextView(appManager);
        tousTextView.setText("Tous");
        linearLayoutSelection.addView(tousTextView);
        linearLayoutSelection.addView(checkAll);


        //but exp
        butExp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!checkable){
                    Tools.textOut(appManager, "Veuillez passer en mode sélection pour exporter");
                    return;
                }


                //generer excel depuis roomlist
                File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/dl");
                dir.mkdirs();
                final File file = new File(dir, "exportExcel.csv");

                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                ArrayList<Stage> selectedStages = new ArrayList<Stage>();

                for(int i=0; i<selectedItems.size(); i++  ) {
                    int pos = Integer.parseInt(selectedItems.get(i));
                    selectedStages.add(stageList.get(pos));
                }

                if(selectedStages.isEmpty()){
                    Tools.textOut(appManager, "Veuillez sélectionner au moins une séance à exporter");
                    return;
                }

                StringBuilder builder = new StringBuilder();

                for(Stage s : selectedStages){


                    builder.append(s.getPerson().getName()+" "+s.getPerson().getFirstname());
                    builder.append(";");
                    builder.append(s.getUser().getName()+" "+s.getUser().getFirstname());
                    builder.append(";");
                    builder.append(s.getToStringCreation());
                    builder.append(";");
                    builder.append(s.getReason());
                    builder.append(";");
                    builder.append(s.getComment());


                    for(Room r : s.getRooms()){

                        builder.append(";");
                        builder.append(""+r.getGame().getName());
                        builder.append(";");
                        builder.append(""+r.getScore());
                        builder.append(";");
                        builder.append(""+r.isUserValidation());

                    }

                    builder.append(System.getProperty("line.separator"));
                }

                try {
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
                    writer.append(builder);
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                EmailTask emailTask = new EmailTask(appManager, file, appManager.getCurrentUser().getEmail(), false);
                emailTask.execute();

                Tools.textOut(appManager, "Exportation terminée");
            }
        });

        //but imp
        buttonImp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!checkable){
                    Tools.textOut(appManager, "Veuillez passer en mode sélection pour imprimer");
                    return;
                }

                ArrayList<Stage> selectedStages = new ArrayList<Stage>();

                for(int i=0; i<selectedItems.size(); i++  ) {
                    int pos = Integer.parseInt(selectedItems.get(i));
                    selectedStages.add(stageList.get(pos));
                }

                if(selectedStages.isEmpty()){
                    Tools.textOut(appManager, "Veuillez sélectionner au moins une séance à exporter");
                    return;
                }

                appManager.export(selectedStages, true);
            }
        });


        //listView listener & adapter
        stageListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Stage stage = stageList.get(position);
                appManager.confHistoDetail(stage);
            }
        });

        stageListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                checkable = true;
                addSelectionCount();
                actualiseListView();
                return false;
            }
        });
        stageListView.setAdapter(new ListAdapter(appManager, stageList));

        updateBar(0);

        //init search layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(appManager,
                R.array.choices, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerChoix.setAdapter(adapter);
        spinnerChoix.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {
                while(searchBar_stage.getChildCount() > 1){
                    searchBar_stage.removeViewAt(searchBar_stage.getChildCount()-1);
                }

                updateBar(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return thisView;
    }

    private void updateBar(final int position){


        moveList(position);

            Button buttonDate = new Button(appManager);
            datePickerFragment.setHandled(buttonDate);
            searchBar_stage.addView(buttonDate);

            buttonDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    datePickerFragment.show(getFragmentManager(), "Date");
                }
            });

            buttonDate.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {}
                @Override
                public void afterTextChanged(Editable s) {
                   moveList(position);
                }
            });


    }

    private void moveList(int position){
        try {
            stageList.clear();
            stageList.addAll(dbManager.getDaoStage().queryForAll());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        for(int i=0; i<stageList.size(); i++){
            int pos = stageList.get(i).compareTo(datePickerFragment.getCalendar().getTime());
            if(position == 0){
                if(pos > 0){
                    stageList.remove(i);
                    i=-1;
                }
            }
            else if(position == 1){
                if(pos < 0){
                    stageList.remove(i);
                    i=-1;
                }
            }

        }

        if(stageList.isEmpty()){
            ((ListAdapter)stageListView.getAdapter()).clear();
            Tools.textOut(appManager, "Aucune donnée");
        }

        actualiseListView();
    }

    private  void addSelectionCount(){
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1);
        layoutParams.gravity = Gravity.RIGHT;
        linearLayoutSelection.setGravity(Gravity.RIGHT);
        linearLayoutSelection.setLayoutParams(layoutParams);
        searchBar_stage.addView(linearLayoutSelection);
    }

    public void removeSelectionCount(){
        searchBar_stage.removeView(linearLayoutSelection);
        checkAll.setChecked(false);
        setCheckable(false);
        actualiseListView();
    }

    // adapter
    public class ListAdapter extends ArrayAdapter {
        private Context context;
        private List<Stage> data;

        public ListAdapter(Context context, List<Stage> data){
            super(context, -1, data);
            this.context = context;
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.histo_item, parent, false);
                final Stage stage = stageList.get(position);
                TextView stagePerson = (TextView) row.findViewById(R.id.histo_stage_person);
                TextView stageUser = (TextView) row.findViewById(R.id.histo_stage_user);
                TextView stageDate = (TextView) row.findViewById(R.id.histo_stage_date);
                TextView stageReason = (TextView) row.findViewById(R.id.histo_stage_reason);
                TextView stageComment = (TextView) row.findViewById(R.id.histo_stage_comment);
                LinearLayout histo_last_layout = (LinearLayout) row.findViewById(R.id.histo_last_layout);

                if (stage.isCreated()) {

                    if(checkable){
                        CheckBox check = new CheckBox(appManager);
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 0, 1);
                        layoutParams.gravity = Gravity.CENTER;
                        check.setLayoutParams(layoutParams);

                        check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                if(isChecked){
                                    if(!selectedItems.contains(String.valueOf(position))) selectedItems.add(String.valueOf(position));
                                }
                                else{
                                    selectedItems.remove(String.valueOf(position));
                                }
                            }
                        });

                        if(selectedItems.contains(String.valueOf(position))) check.setChecked(true);
                        else check.setChecked(false);
                        check.setId(R.id.checkbox_id);
                        histo_last_layout.setWeightSum(3);
                        histo_last_layout.addView(check);
                    }

                    stagePerson.setText(stage.getPerson().getFirstname() + " - " + stage.getPerson().getName());
                    stageUser.setText(stage.getUser().getFirstname() + " - " + stage.getUser().getName());
                    stageDate.setText(stage.getToStringCreation());
                    stageReason.setText(stage.getReason());
                    stageComment.setText(stage.getComment());
                }

            return row;
        }
    }

    public void actualiseListView(){
        ((ListAdapter) stageListView.getAdapter()).notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        spinnerChoix.setSelection(0);
        datePickerFragment = new DatePickerFragment();
    }

    public boolean isCheckable() {
        return checkable;
    }

    public void setCheckable(boolean checkable) {
        this.checkable = checkable;
    }
}
