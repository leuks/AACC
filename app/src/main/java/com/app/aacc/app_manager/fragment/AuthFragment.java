package com.app.aacc.app_manager.fragment;

import android.app.Fragment;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.text.Selection;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.app.aacc.app_manager.AppManager;
import com.app.aacc.R;

import java.sql.SQLException;

import Entities.Tools;

/**
 * Created by leuks on 02/07/2016.
 */
public class AuthFragment extends Fragment{
    private EditText passwordField;
    private EditText loginField;
    private AppManager appManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.appManager = (AppManager) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.auth_fragment_app_manager, container, false);
        this.passwordField = (EditText) thisView.findViewById(R.id.password);
        this.loginField = ((EditText) thisView.findViewById(R.id.login));
        final Button submitButton = (Button) thisView.findViewById(R.id.submit);
        final Button manualButton = (Button) thisView.findViewById(R.id.manual_button);


        Drawable eyeDrawable = getResources().getDrawable(R.drawable.eye, appManager.getTheme());
        passwordField.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, eyeDrawable, null);
        //passwordField.setSe

        passwordField.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                passwordField.requestFocus();

                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    if(event.getRawX() >= (passwordField.getRight() - passwordField.getCompoundDrawables()[2].getBounds().width())) {
                        passwordField.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        return true;
                    }

                }
                else if(event.getAction() == MotionEvent.ACTION_UP){
                    passwordField.setInputType(129);
                }
                return false;
            }
        });

        //manual
        manualButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appManager.show(AppManager.ROAD_MANUAL, true);
            }
        });
        // button
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String login = loginField.getText().toString().trim();
                String password = passwordField.getText().toString().trim();

                Tools.changeStatusEdit(false, loginField, appManager);
                Tools.changeStatusEdit(false, passwordField, appManager);

                if(login.isEmpty()){
                    Tools.changeStatusEdit(true, loginField, appManager);
                }
                else if(password.isEmpty()){
                    Tools.changeStatusEdit(true, passwordField, appManager);
                }
                else {
                    try {
                        int result = appManager.getHelper().testExists(login, password);
                        switch (result) {
                            case 0:
                                appManager.initUser();
                                appManager.show(AppManager.ROAD_AFTER_START, true);
                                break;
                            case 1:
                                Tools.changeStatusEdit(true, loginField, appManager);
                                break;
                            case 2:
                                Tools.changeStatusEdit(true, passwordField, appManager);
                                break;
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        return  thisView;
    }

    @Override
    public void onResume() {
        super.onResume();
        //clear field
        passwordField.getText().clear();
        loginField.getText().clear();
    }
}
