package com.app.aacc.app_manager.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.app.aacc.R;
import com.app.aacc.app_manager.AppManager;

import java.util.ArrayList;
import java.util.List;

import bdd.Room;
import bdd.Stage;

/**
 * Created by leuks on 18/07/2016.
 */
public class HistoriqueDetailFragment extends Fragment{

    private AppManager appManager;
    private ListView roomListView;
    private List<Room> roomList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.appManager = (AppManager) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.historique_detail_app_manager, container, false);
        this.roomListView = (ListView) thisView.findViewById(R.id.roomList);

        //listView listener & adapter
        roomListView.setAdapter(new ListAdapter(appManager, roomList));

        return thisView;
    }

    // adapter
    private class ListAdapter extends ArrayAdapter {
        private Context context;
        private List<Room> data;

        public ListAdapter(Context context, List<Room> data){
            super(context, -1, data);
            this.context = context;
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.histo_detail_item, parent, false);
            Room room = roomList.get(position);
            TextView roomScore = (TextView) row.findViewById(R.id.histo_detail_score);
            TextView roomGame = (TextView) row.findViewById(R.id.histo_detail_game);
            TextView roomValidation = (TextView) row.findViewById(R.id.histo_detail_validation);

            roomScore.setText(String.valueOf(room.getScore()));
            roomGame.setText(room.getGame().getName());
            if(room.isUserValidation()) roomValidation.setText("Oui");
            else roomValidation.setText("Non");


            return row;
        }
    }

    public void setRoomList(ArrayList<Room> roomList){
        this.roomList = roomList;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((ListAdapter) roomListView.getAdapter()).notifyDataSetChanged();
    }
}
