package com.app.aacc.app_manager.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.app.aacc.app_manager.AppManager;
import com.app.aacc.R;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.wdullaer.swipeactionadapter.SwipeActionAdapter;
import com.wdullaer.swipeactionadapter.SwipeDirection;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import Entities.Tools;
import ar.com.daidalos.afiledialog.FileChooserDialog;
import bdd.DBManager;
import bdd.Person;
import com.baoyz.swipemenulistview.SwipeMenuListView;
/**
 * Created by leuks on 04/07/2016.
 */
public class UserMenuFragment extends Fragment {
    private ArrayList<Person> personList;
    private List<Person> originalPersonList;
    private SwipeMenuListView personListView;
    private ListAdapter listAdapter;
    private EditText searchField;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.user_menu_fragment_app_manager, container, false);
        final Button addButton = (Button) thisView.findViewById(R.id.morePerson);
        searchField = (EditText) thisView.findViewById(R.id.searchPerson);
        final Button importButton = (Button) thisView.findViewById(R.id.import_button);
        final AppManager appManager = (AppManager) getActivity();
        final DBManager dbManager = appManager.getHelper();
        this.personListView = (SwipeMenuListView) thisView.findViewById(R.id.personList);

        // adapter & listeners
        try {
            originalPersonList = dbManager.getDaoPerson().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        this.personList = new ArrayList(originalPersonList);

        //ListMenuCreator
        SwipeMenuCreator swipeMenuCreator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu swipeMenu) {
                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        appManager.getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth((90));
                // set a icon
                deleteItem.setIcon(R.drawable.remove);
                // add to menu
                swipeMenu.addMenuItem(deleteItem);
            }
        };
        personListView.setMenuCreator(swipeMenuCreator);

        //click menu list listener
        personListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {

                switch (index) {
                    case 0:

                        final Person personToDelete = personList.get(position);

                        //personListView.smoothOpenMenu(position);

                        AlertDialog dialog = null;
                        AlertDialog.Builder builder = new AlertDialog.Builder(appManager);
                        builder.setMessage("Supprimer cette personne ?")
                                .setTitle("Suppresion");
                        builder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dbManager.deletePerson(personToDelete);
                                personList.remove(position);
                                notice();

                                Tools.textOut(appManager, "Utilisateur " + personToDelete.getFirstname() + " - " + personToDelete.getName() + " supprimé");
                            }
                        });
                        builder.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                        dialog = builder.create();
                        dialog.show();
                        break;
                }

                return false; //false/true not close
            }
        });

        //list click listener
        personListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                appManager.setCurrentPerson(personList.get(position));
                appManager.show(AppManager.ROAD_REASON, true);
            }

        });

        //swape authorisation
        personListView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);

            //adapter
        listAdapter = new ListAdapter(appManager, personList);
        personListView.setAdapter(listAdapter);


        // button add
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appManager.show(AppManager.ROAD_NEW_PERSON, true);
            }
        });

        //button import
        importButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FileChooserDialog dialog = new FileChooserDialog(appManager);
                dialog.loadFolder(Environment.DIRECTORY_DOWNLOADS);

                dialog.addListener(new FileChooserDialog.OnFileSelectedListener() {
                    public void onFileSelected(Dialog source, File file) {
                        source.hide();
                        Tools.textOut(appManager, "Fichier sélectionné: " + file.getName());
                        String filename = file.getAbsolutePath();
                        if(filename.contains(".")){
                            String filenameArray[] = filename.split("\\.");
                            String extension = filenameArray[filenameArray.length-1];
                            if(extension.equals("csv")){

                                BufferedReader reader = null;
                                FileInputStream is = null;
                                try {
                                    is = new FileInputStream(file);
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                }

                                reader = new BufferedReader(new InputStreamReader(is));

                                try {
                                    String line = reader.readLine();
                                    String separator = line.contains(",") ? "\\," : "\\;";
                                    while ((line = reader.readLine()) != null) {
                                        String[] RowData = line.split(separator);
                                        String name = RowData[0];
                                        String firstname = RowData[1];
                                        boolean gender = RowData[2].equals("1");
                                        String email = RowData[3];
                                        String phone = RowData[4];
                                        String departement = RowData[5];
                                        boolean cause = RowData[6].equals("1");
                                        Date birth = Tools.stringToDate(RowData[7]);

                                        boolean correct = true;
                                        for(Person p : personList){
                                            if(p.getEmail().equals(email)) correct = false;
                                        }
                                        if(correct){
                                            Person newPerson = new Person(name, firstname, gender, birth, email, phone, departement, cause);
                                            personList.add(newPerson);
                                            dbManager.createPerson(newPerson);
                                            notice();
                                        } else{
                                            Tools.textOut(appManager, "Personne "+firstname+", "+name+" existe déjà");
                                        }
                                    }
                                }
                                catch (IOException ex) {
                                    Tools.textOut(appManager, "Erreur dans l'écriture du fichier CSV");
                                }
                                finally {
                                    try {
                                        is.close();
                                    }
                                    catch (IOException e) {
                                        // handle exception
                                    }
                                }
                            }
                            else{
                                Tools.textOut(appManager, "Le fichier doit être au format CSV");
                            }
                        }
                        else{
                            Tools.textOut(appManager, "Le fichier doit être au format CSV");
                        }
                    }
                    public void onFileSelected(Dialog source, File folder, String name) {
                        source.hide();
                        Tools.textOut(appManager, "Fichier crée: " + folder.getName() + "/" + name);
                    }
                });

                dialog.show();
            }
        });

        //search field
        searchField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                //personList.clear();
                personList.clear();
                String textSearch = searchField.getText().toString();
                for(Person person : originalPersonList){
                    if(person.getFirstname().toLowerCase().contains(textSearch.toLowerCase()) || person.getName().toLowerCase().contains(textSearch.toLowerCase())){
                        personList.add(person);
                    }
                }
                notice();
            }
        });
        return thisView;
    }

    @Override
    public void onResume() {
        super.onResume();
        searchField.getText().clear();
    }

    // adapter
    private class ListAdapter extends ArrayAdapter {
        private Context context;
        private List<Person> data;

        public ListAdapter(Context context, List<Person> data){
            super(context, -1, data);
            this.context = context;
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.person_item_app_manager, parent, false);
            Person person = data.get(position);
            TextView firstname = (TextView) row.findViewById(R.id.firstname_item_person);
            TextView name = (TextView) row.findViewById(R.id.name_item_person);
            TextView birth = (TextView) row.findViewById(R.id.birth_item_person);
            ImageView gender = (ImageView) row.findViewById(R.id.gender_item_person);

            firstname.setText(person.getFirstname());
            name.setText(person.getName());
            birth.setText(person.getToStringBirth());
            if(person.getGender()) gender.setImageResource(R.drawable.female);
            else gender.setImageResource(R.drawable.male);

            return row;
        }
    }

    private void notice(){
        listAdapter.notifyDataSetChanged();
        personListView.setAdapter(listAdapter);
    }
}
