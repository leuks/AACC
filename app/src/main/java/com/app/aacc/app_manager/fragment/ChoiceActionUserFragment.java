package com.app.aacc.app_manager.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.app.aacc.R;
import com.app.aacc.app_manager.AppManager;

/**
 * Created by leuks on 17/07/2016.
 */
public class ChoiceActionUserFragment extends Fragment {
    private AppManager appManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.appManager = (AppManager) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.choice_action_user_fragment_app_manager, container, false);
        final Button buttonStart = (Button) thisView.findViewById(R.id.button_start_choice_user);
        final Button buttonHisto = (Button) thisView.findViewById(R.id.button_histo_choice_user);

        //button start
        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appManager.show(AppManager.ROAD_USER, true);
            }
        });

        //button histo
        buttonHisto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appManager.show(AppManager.ROAD_HISTORIQUE, true);
            }
        });

        return thisView;
    }
}
