package com.app.aacc.app_manager;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.widget.Toast;

import com.app.aacc.R;
import com.app.aacc.app_manager.fragment.AdminMenuFragment;
import com.app.aacc.app_manager.fragment.AuthFragment;
import com.app.aacc.app_manager.fragment.ChoiceActionUserFragment;
import com.app.aacc.app_manager.fragment.ChooseGameFragment;
import com.app.aacc.app_manager.fragment.ChooseGameParamsFragment;
import com.app.aacc.app_manager.fragment.CongratulationFragment;
import com.app.aacc.app_manager.fragment.HistoriqueDetailFragment;
import com.app.aacc.app_manager.fragment.HistoriqueFragment;
import com.app.aacc.app_manager.fragment.ManageUserFragment;
import com.app.aacc.app_manager.fragment.ManualFragment;
import com.app.aacc.app_manager.fragment.NewPersonFragment;
import com.app.aacc.app_manager.fragment.NewUserFragment;
import com.app.aacc.app_manager.fragment.ParamFragment;
import com.app.aacc.app_manager.fragment.ReasonFragment;
import com.app.aacc.app_manager.fragment.UserMenuFragment;
import com.app.aacc.image_game.ImageGame;
import com.app.aacc.image_game.fragment.ScoreFragment;
import com.app.aacc.sign_game.SignGame;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.FontSelector;
import com.itextpdf.text.pdf.PdfWriter;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import Entities.EmailTask;
import Entities.Tools;
import bdd.DBManager;
import bdd.Game;
import bdd.Person;
import bdd.Room;
import bdd.Stage;
import bdd.User;

public class AppManager extends OrmLiteBaseActivity<DBManager> {
    private AuthFragment authFragment;
    private UserMenuFragment userMenuFragment;
    private NewPersonFragment newPersonFragment;
    private ReasonFragment reasonFragment;
    private ChooseGameFragment chooseGameFragment;
    private AdminMenuFragment adminMenuFragment;
    private ManageUserFragment manageUserFragment;
    private NewUserFragment newUserFragment;
    private CongratulationFragment congratulationFragment;
    private ChooseGameParamsFragment chooseGameParamsFragment;
    private ParamFragment paramFragment;
    private ChoiceActionUserFragment choiceActionUserFragment;
    private HistoriqueFragment historiqueFragment;
    private HistoriqueDetailFragment historiqueDetailFragment;
    private ScoreFragment scoreFragmentImage;
    private ManualFragment manualFragment;
    private com.app.aacc.sign_game.fragment.ScoreFragment  scoreFragmentSign;
    private DBManager dbManager;
    private int actualGame;
    private int actualShow;


    private Stage currentStage;

    public static final int ROAD_START = 0;
    public static final int ROAD_AFTER_START = 1;
    public static final int ROAD_NEW_PERSON = 2;
    public static final int ROAD_REASON = 3;
    public static final int ROAD_CHOOSE_GAME = 4;
    public static final int ROAD_ADMIN = 5;
    public static final int ROAD_USER = 6;
    public static final int ROAD_MANAGE_ACCOUNT = 7;
    public static final int ROAD_MANAGE_GAME_PARAM = 8;
    public static final int ROAD_NEW_USER = 9;
    public static final int ROAD_PARAMS_IMAGE_GAME = 10;
    public static final int ROAD_CHOICE_USER = 11;
    public static final int ROAD_HISTORIQUE = 12;
    public static final int ROAD_HISTORIQUE_DETAIL = 13;
    public static final int ROAD_PARAMS_SIGN_GAME = 14;
    public static final int ROAD_CONGRATULATION = 15;
    public static final int ROAD_MANUAL = 16;


    @Override
    public void onBackPressed() {
        if(historiqueFragment != null && historiqueFragment.isCheckable()){
            historiqueFragment.removeSelectionCount();
        }
        else super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("currentStage", this.currentStage);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        setContentView(R.layout.activity_app_manager);

        this.dbManager = getHelper();
        dbManager.initDao();
        this.authFragment = new AuthFragment();
        this.userMenuFragment = new UserMenuFragment();
        this.newPersonFragment = new NewPersonFragment();
        this.reasonFragment = new ReasonFragment();
        this.chooseGameFragment = new ChooseGameFragment();
        this.adminMenuFragment = new AdminMenuFragment();
        this.manageUserFragment = new ManageUserFragment();
        this.congratulationFragment = new CongratulationFragment();
        this.newUserFragment = new NewUserFragment();
        this.chooseGameParamsFragment = new ChooseGameParamsFragment();
        this.paramFragment = new ParamFragment();
        this.choiceActionUserFragment = new ChoiceActionUserFragment();
        this.historiqueFragment = new HistoriqueFragment();
        this.historiqueDetailFragment = new HistoriqueDetailFragment();
        this.manualFragment = new ManualFragment();


        if (savedInstanceState != null) {
            if(savedInstanceState.get("currentStage") != null){
                this.currentStage = (Stage) savedInstanceState.get("currentStage");
                dbManager.setCurrentUser(currentStage.getUser());
            }
            return;
        }

        getFragmentManager().beginTransaction().add(R.id.frameLayoutAppManager, authFragment).commit();

    }

    public void confHistoDetail(Stage stage){
        historiqueDetailFragment.setRoomList(new ArrayList<Room>(stage.getRooms()));
        show(ROAD_HISTORIQUE_DETAIL, true);
    }

    public void initUser(){
        this.currentStage = new Stage(dbManager);
        User user = getHelper().getCurrentUser();
        setCurrentUser(user);
    }

    // roads
    public void show(int direction, boolean back){
        boolean redirection =false;
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        switch (direction){
            case ROAD_START:
                fragmentTransaction.replace(R.id.frameLayoutAppManager, authFragment);
                break;
            case ROAD_AFTER_START:
                redirection = true;
                User user = getHelper().getCurrentUser();
                if(user.getAdmin()) show(ROAD_ADMIN, back);
                else show(ROAD_CHOICE_USER, back);
                break;
            case ROAD_NEW_PERSON:
                fragmentTransaction.replace(R.id.frameLayoutAppManager, newPersonFragment);
                break;
            case ROAD_REASON:
                fragmentTransaction.replace(R.id.frameLayoutAppManager, reasonFragment);
                break;
            case ROAD_CHOOSE_GAME:
                fragmentTransaction.replace(R.id.frameLayoutAppManager, chooseGameFragment);
                break;
            case ROAD_ADMIN:
                fragmentTransaction.replace(R.id.frameLayoutAppManager, adminMenuFragment);
                break;
            case ROAD_USER:
                fragmentTransaction.replace(R.id.frameLayoutAppManager, userMenuFragment);
                break;
            case ROAD_MANAGE_ACCOUNT:
                fragmentTransaction.replace(R.id.frameLayoutAppManager, manageUserFragment);
                break;
            case ROAD_MANAGE_GAME_PARAM:
                fragmentTransaction.replace(R.id.frameLayoutAppManager, chooseGameParamsFragment);
                break;
            case ROAD_NEW_USER:
                fragmentTransaction.replace(R.id.frameLayoutAppManager, newUserFragment);
                break;
            case ROAD_PARAMS_IMAGE_GAME:
                paramFragment.init(Game.IMAGE);
                fragmentTransaction.replace(R.id.frameLayoutAppManager, paramFragment);
                break;
            case ROAD_PARAMS_SIGN_GAME:
                paramFragment.init(Game.SIGN);
                fragmentTransaction.replace(R.id.frameLayoutAppManager, paramFragment);
                break;
            case ROAD_CHOICE_USER:
                fragmentTransaction.replace(R.id.frameLayoutAppManager, choiceActionUserFragment);
                break;
            case ROAD_HISTORIQUE:
                fragmentTransaction.replace(R.id.frameLayoutAppManager, historiqueFragment);
                break;
            case ROAD_HISTORIQUE_DETAIL:
                fragmentTransaction.replace(R.id.frameLayoutAppManager, historiqueDetailFragment);
                break;
            case ROAD_CONGRATULATION:
                fragmentTransaction.replace(R.id.frameLayoutAppManager, congratulationFragment);
                break;
            case ROAD_MANUAL:
                fragmentTransaction.replace(R.id.frameLayoutAppManager, manualFragment);
                break;
        }
        if(!redirection) {
            if (back) fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

    public void showSpecial(Fragment direction) {
        getFragmentManager().beginTransaction().replace(R.id.frameLayoutAppManager, direction).addToBackStack(null).commit();
        Tools.textOut(this, ""+getFragmentManager().getBackStackEntryCount());
    }

    public void createRooms(ArrayList<Game> gameArrayOrdered){
        this.currentStage.getRooms().clear();
        int count = 0;
        actualShow = 0;
        for(Game game : gameArrayOrdered){
            this.currentStage.getRooms().add(new Room(game, currentStage, dbManager, count));
            count++;
        }
        startTests();
    }

    private void startTests(){
        ArrayList<Room> roomList = new ArrayList(this.currentStage.getRooms());
            Room instRoom = roomList.get(actualGame);
            String gameName = instRoom.getGame().getName();
            Intent i = null;
            switch(gameName) {
                case Game.IMAGE:
                    i = new Intent(this, ImageGame.class);
                    i.putExtra("game", instRoom.getGame());
                    break;
                case Game.SIGN:
                    i = new Intent(this, SignGame.class);
                    i.putExtra("game", instRoom.getGame());
                    break;

            }
           startActivityForResult(i, 1);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        ArrayList<Room> roomList = new ArrayList(this.currentStage.getRooms());
            if (resultCode == Activity.RESULT_OK) {

                String name = data.getExtras().getString("name");
                String score = null;
                Object tamps = null;
                switch(name){
                    case Game.IMAGE:
                        ArrayList<Integer> scores = data.getExtras().getIntegerArrayList("scores");

                        int sum = 0;
                        for(int sc : scores){
                            sum+=sc;
                        }
                        score = String.valueOf(sum/scores.size());
                        tamps = scores;
                        break;

                    case Game.SIGN:
                        score = data.getExtras().getInt("score_good")+":"+data.getExtras().getInt("nb_good")+"/"+data.getExtras().getInt("score_bad")+":"+data.getExtras().getInt("nb_bad")+"/"+data.getExtras().getLong("moy_time");
                }

                for(Room room : roomList){
                    if(room.getGame().getName().equals(name)){
                        room.setScore(score);
                        room.setTamps(tamps);
                    }
                }

                actualGame++;
                dbManager.createOrUpdateStage(this.currentStage);

                if(actualGame == roomList.size()){
                    show(ROAD_CONGRATULATION, true);
                }
                else{
                    Tools.textOut(this, "Jeu suivant");
                    startTests();
                }

            }
    }

    public void showScore(Boolean userValidation){

        if(actualShow != 0){
            Room roomToChange = (Room) new ArrayList(currentStage.getRooms()).get(actualShow-1);
            roomToChange.setUserValidation(userValidation);
            dbManager.createOrUpdateStage(this.currentStage);
        }

        if(actualShow == currentStage.getRooms().size()){
            ArrayList toPrint = new ArrayList();
            toPrint.add(currentStage);
            export(toPrint, false);

            getFragmentManager().popBackStackImmediate();
            getFragmentManager().popBackStackImmediate();
            getFragmentManager().popBackStackImmediate();
            getFragmentManager().popBackStackImmediate();

            if(currentStage.getUser().getAdmin()) getFragmentManager().popBackStackImmediate();

            for(Room room : currentStage.getRooms()) getFragmentManager().popBackStackImmediate();

            Tools.textOut(this, "Séance enregistrée");
        }
        else{
            Room thisRoom = (Room) new ArrayList(currentStage.getRooms()).get(actualShow);

            switch (thisRoom.getGame().getName()){
                case Game.IMAGE:
                    scoreFragmentImage = new ScoreFragment();
                    scoreFragmentImage.setScore(Double.parseDouble(thisRoom.getScore()),(ArrayList) thisRoom.getTamps() , Integer.parseInt(dbManager.getParameters(Game.IMAGE).get("score")));
                    actualShow++;
                    showSpecial(scoreFragmentImage);
                    break;
                case Game.SIGN:
                    scoreFragmentSign = new com.app.aacc.sign_game.fragment.ScoreFragment();
                    String[] scoreFromRoom = thisRoom.getScore().split("\\/");

                    int nbGood = Integer.parseInt(scoreFromRoom[0].split("\\:")[0]);
                    int nbBad = Integer.parseInt(scoreFromRoom[1].split("\\:")[0]);
                    int moyTime = Integer.parseInt(scoreFromRoom[2]);
                    int displayerCount = Integer.parseInt(scoreFromRoom[0].split("\\:")[1]);
                    int displayerErrCount = Integer.parseInt(scoreFromRoom[1].split("\\:")[1]);

                    ArrayList vals = new ArrayList();
                    ArrayList validators = new ArrayList();

                    vals.add(nbGood);
                    vals.add(nbBad);
                    validators.add(Integer.parseInt(dbManager.getParameters(Game.SIGN).get("score_good")));
                    validators.add(Integer.parseInt(dbManager.getParameters(Game.SIGN).get("score_bad")));

                    scoreFragmentSign.setScore(moyTime, vals, validators, displayerCount, displayerErrCount);
                    actualShow++;

                    showSpecial(scoreFragmentSign);
                    break;
            }
        }
    }

    public void export(ArrayList<Stage> stagesToPrint, boolean mode) {

        Document doc = new Document();
        File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/dl");
        dir.mkdirs();
        File file = null;

        if(stagesToPrint.size() == 1){
            file = new File(dir, "Séance"+stagesToPrint.get(0).getId()+".pdf");
        }
        else{
            int id1 = stagesToPrint.get(0).getId();
            int id2 = stagesToPrint.get(stagesToPrint.size()-1).getId();
            file = new File(dir, "Séance"+id1+"a"+id2+".pdf");

        }

        //file
        try {
            file.createNewFile();
            PdfWriter.getInstance(doc, new FileOutputStream(file));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }

        //images

        Image imageLogo=null;
        Image checkedImage=null;
        Image noCheckedImage=null;


        try {
            Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.logo_pdf);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            imageLogo = Image.getInstance(stream.toByteArray());
            imageLogo.setAlignment(Image.ALIGN_CENTER | Image.TEXTWRAP);
            imageLogo.setBorder(Image.BOX);
            stream.close();

            //check
            bmp = BitmapFactory.decodeResource(getResources(), R.drawable.check);
            stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            checkedImage = Image.getInstance(stream.toByteArray());
            checkedImage.setAlignment(Image.ALIGN_CENTER | Image.TEXTWRAP);
            checkedImage.setBorder(Image.BOX);
            stream.close();

            //noCheck
            bmp = BitmapFactory.decodeResource(getResources(), R.drawable.nocheck);
            stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            noCheckedImage = Image.getInstance(stream.toByteArray());
            noCheckedImage.setAlignment(Image.ALIGN_CENTER | Image.TEXTWRAP);
            noCheckedImage.setBorder(Image.BOX);
            stream.close();
        } catch (BadElementException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //start
        doc.open();

        for(int i=0; i<stagesToPrint.size();i++) {

            Stage s = stagesToPrint.get(i);

            int time = 0;
            boolean pass = true;
            for (Room room : s.getRooms()) {
                switch (room.getGame().getName()) {
                    case Game.IMAGE:
                        time += 3;
                        break;
                    case Game.SIGN:
                        HashMap<String, String> map = dbManager.getParameters(Game.SIGN);
                        Double dlb = Double.parseDouble(map.get("total_time"));
                        time += dlb.intValue();
                        break;
                }
                if (!room.isUserValidation()) pass = false;
            }


            try {

                if(i != 0){
                    doc.newPage();
                }

                //page 1

                // image logo pdf
                doc.add(imageLogo);

                Paragraph paragraph = new Paragraph(new Phrase("Résultats des tests psychotechniques"));
                paragraph.setAlignment(Element.ALIGN_CENTER);
                doc.add(paragraph);

                paragraph = new Paragraph(new Phrase("Nom et Prénom: " + s.getPerson().getName() + " " + s.getPerson().getFirstname()));
                paragraph.setAlignment(Element.ALIGN_LEFT);
                doc.add(paragraph);


                paragraph = new Paragraph(new Phrase("Date de naissance: " + s.getPerson().getToStringBirth()));
                paragraph.setAlignment(Element.ALIGN_LEFT);
                doc.add(paragraph);

                paragraph = new Paragraph(new Phrase("Le candidat a été reçu ce jour le " + s.getToStringCreation()
                        + " par " + s.getUser().getName() + " " + s.getUser().getFirstname() + "."));
                paragraph.setAlignment(Element.ALIGN_LEFT);
                doc.add(paragraph);

                paragraph = new Paragraph(new Phrase("L'ensemble des tests ont duré: " + time + " minutes."));
                paragraph.setAlignment(Element.ALIGN_LEFT);
                doc.add(paragraph);


                if (pass) {
                    paragraph = new Paragraph(new Phrase("Le candidat  sait exprimer sa responsabilité lors des infractions commises. Il n’a pas de\n" +
                            "difficultés pendant la passation, il comprend bien les consignes et ne montre pas de difficulté\n" +
                            "de concentration."));
                } else {
                    paragraph = new Paragraph(new Phrase("Le candidat n'a pas passé tous les tests"));
                }
                paragraph.setAlignment(Element.ALIGN_LEFT);
                doc.add(paragraph);


                paragraph = new Paragraph(new Phrase("Résultats par tests: "));
                paragraph.setAlignment(Element.ALIGN_LEFT);
                doc.add(paragraph);

                boolean generalJob = true;
                for (Room room : s.getRooms()) {
                    switch (room.getGame().getName()) {
                        case Game.SIGN:
                            //from score
                            String roomScore = room.getScore();
                            String[] tabScore = roomScore.split("/");
                            int totGood = Integer.parseInt(tabScore[0].split("\\:")[1]);
                            int totBad = Integer.parseInt(tabScore[1].split("\\:")[1]);
                            int scoreGood = Integer.parseInt(tabScore[0].split("\\:")[0]);
                            int scoreBad = Integer.parseInt(tabScore[1].split("\\:")[0]);
                            Long moyTime = Long.parseLong(tabScore[2]);
                            boolean validationSign = room.isUserValidation();

                            if (!validationSign) generalJob = false;

                            //from params
                            HashMap<String, String> paramsSign = dbManager.getParameters(Game.SIGN);
                            int scoreGoodWanted = Integer.parseInt(paramsSign.get("score_good"));
                            int scoreBadWanted = Integer.parseInt(paramsSign.get("score_bad"));
                            double perGood = (totGood * (((double) scoreGoodWanted) / 100));
                            double perBad = (totBad * (((double) scoreBadWanted) / 100));

                            //final
                            boolean isGood = scoreGood >= perGood;
                            boolean isBad = scoreBad <= perBad;
                            int perScoreGood;
                            if (scoreGood == 0) {
                                perScoreGood = 0;
                            } else {
                                perScoreGood = ((scoreGood * 100) / totGood);
                            }

                            int perScoreBad;
                            if (scoreBad == 0) {
                                perScoreBad = 0;
                            } else {
                                perScoreBad = ((scoreBad * 100) / totBad);
                            }


                            paragraph = new Paragraph(new Phrase("Temps de réaction"));
                            paragraph.setAlignment(Element.ALIGN_CENTER);
                            doc.add(paragraph);

                            paragraph = new Paragraph("Le temps de réaction représente la capacité à réagir le plus rapidement possible et de\n" +
                                    "manière ciblée à un ou plusieurs stimuli.\n" +
                                    "Résultats :\n" +
                                    "Le candidat  présente un temps moyen de réaction  de " + moyTime + " ms\n");

                            paragraph.add(new Phrase("Le test a été accompli avec " + perScoreGood + "% de bons points et " + perScoreBad + "% d'érreurs\n\n"));
                            paragraph.add(new Phrase("Il est donc dans la moyenne attendue: "));

                            if (isGood && isBad && validationSign) {
                                paragraph.add(new Chunk(checkedImage, 0, 0));
                                paragraph.add(new Phrase(" Oui   "));
                                paragraph.add(new Chunk(noCheckedImage, 0, 0));
                                paragraph.add(new Phrase(" Non\n\n"));
                            } else {
                                paragraph.add(new Chunk(noCheckedImage, 0, 0));
                                paragraph.add(new Phrase(" Oui   "));
                                paragraph.add(new Chunk(checkedImage, 0, 0));
                                paragraph.add(new Phrase(" Non\n\n"));
                            }

                            doc.add(paragraph);

                            break;
                        case Game.IMAGE:
                            HashMap<String, String> params = dbManager.getParameters(Game.IMAGE);
                            double score = Double.parseDouble(room.getScore());
                            int wantedPerScore = Integer.parseInt(params.get("score"));

                            boolean validationImage = room.isUserValidation();

                            if (!validationImage) generalJob = false;

                            paragraph = new Paragraph("Capacité attentionnelle");
                            paragraph.setAlignment(Element.ALIGN_CENTER);
                            doc.add(paragraph);

                            paragraph = new Paragraph("La capacité de mémorisation et d’attention peut être mise en évidence avec ce test où des\n" +
                                    "photos apparaissent relativement rapidement et pour lesquelles l’usager doit indiquer tous\n" +
                                    "les éléments qui s’y trouvaient.\n" +
                                    "Résultats:\n" +
                                    "Le candidtat présente une capacité attentionnelle    ");
                            if (score > wantedPerScore) {
                                paragraph.add(new Chunk(checkedImage, 0, 0));
                                paragraph.add(new Phrase(" Satisfaisante   "));
                                paragraph.add(new Chunk(noCheckedImage, 0, 0));
                                paragraph.add(new Phrase(" Insatisfaisante\n"));
                            } else {
                                paragraph.add(new Chunk(noCheckedImage, 0, 0));
                                paragraph.add(new Phrase(" Satisfaisante   "));
                                paragraph.add(new Chunk(checkedImage, 0, 0));
                                paragraph.add(new Phrase(" Insatisfaisante\n"));
                            }

                            paragraph.add(new Phrase("Soit un rang percentile de: " + score + " %\n"));

                            paragraph.add(new Phrase("Il est donc dans la moyenne attendue:   "));
                            if (validationImage) {
                                paragraph.add(new Chunk(checkedImage, 0, 0));
                                paragraph.add(new Phrase(" Oui   "));
                                paragraph.add(new Chunk(noCheckedImage, 0, 0));
                                paragraph.add(new Phrase(" Non\n\n"));
                            } else {
                                paragraph.add(new Chunk(noCheckedImage, 0, 0));
                                paragraph.add(new Phrase(" Oui   "));
                                paragraph.add(new Chunk(checkedImage, 0, 0));
                                paragraph.add(new Phrase(" Non\n\n"));
                            }

                            paragraph.setAlignment(Element.ALIGN_LEFT);
                            doc.add(paragraph);
                            break;
                    }
                }

                FontSelector selector = new FontSelector();
                Font f1 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 12);
                f1.setColor(BaseColor.RED);
                selector.addFont(f1);
                Phrase ph = selector.process("\n\nLe candidat obtient des résultats favorables pour la\n" +
                        "conduite automobile.\n\n");

                paragraph = new Paragraph(ph);

                if (generalJob) {
                    paragraph.add(new Chunk(checkedImage, 0, 0));
                    paragraph.add(new Phrase(" Oui   "));
                    paragraph.add(new Chunk(noCheckedImage, 0, 0));
                    paragraph.add(new Phrase(" Non\n"));
                } else {
                    paragraph.add(new Chunk(noCheckedImage, 0, 0));
                    paragraph.add(new Phrase(" Oui   "));
                    paragraph.add(new Chunk(checkedImage, 0, 0));
                    paragraph.add(new Phrase(" Non\n"));
                }
                paragraph.setAlignment(Element.ALIGN_CENTER);
                doc.add(paragraph);

                paragraph = new Paragraph(s.getUser().getName() + " " + s.getUser().getFirstname());
                paragraph.setAlignment(Element.ALIGN_LEFT);
                doc.add(paragraph);

            } catch (DocumentException e) {
                e.printStackTrace();
            }


        }

        doc.addTitle("Seance" + file.getName());
        doc.addKeywords("Java, PDF, iText");
        doc.addAuthor("aaccApp");
        doc.addCreator("aaccApp");

        doc.close();

        if(!mode) {
            EmailTask emailTask = new EmailTask(this, file, currentStage.getUser().getEmail(), true);
            emailTask.execute();
        }

        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.fromFile(file),"application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        Intent intent = Intent.createChooser(target, "Imprimer le PDF");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getApplicationContext(), "Pas de visionneuse ou d'imprimante", Toast.LENGTH_SHORT).show();
        }

    }

        //GS
    public AuthFragment getAuthFragment() {
        return authFragment;
    }
    public void setCurrentPerson(Person person){
        actualGame = 0;
        if(currentStage.getPerson() != null){
            User user = getCurrentUser();
            currentStage = new Stage(dbManager);
            currentStage.setUser(user);
            currentStage.setPerson(person);
        }
        else    this.currentStage.setPerson(person);

    }
    public void setCurrentReason(String reason, String comment){
        this.currentStage.setReason(reason);
        this.currentStage.setComment(comment);
    }
    public void setCurrentUser(User user){this.currentStage.setUser(user);}
    public User getCurrentUser(){ return this.currentStage.getUser();}
    public String getCurrentReason(){ return this.currentStage.getReason();}
    public Person getCurrentPerson(){ return this.currentStage.getPerson();}

}
