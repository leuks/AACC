package com.app.aacc.app_manager.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.app.aacc.app_manager.AppManager;
import com.app.aacc.R;

import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import Entities.Tools;
import bdd.Person;

/**
 * Created by leuks on 04/07/2016.
 */
public class NewPersonFragment extends Fragment {
    private EditText firstnameField;
    private EditText nameField;
    private EditText emailField;
    private EditText phoneField;
    private EditText departementField;
    private RadioGroup genderField;
    private RadioGroup causeField;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.create_person_fragment_app_manager, container, false);
        final Button buttonNewSubmit = (Button) thisView.findViewById(R.id.new_person_submit);
        final Button buttonNewBirth = (Button) thisView.findViewById(R.id.new_person_birth);
        final DatePickerFragment datePickerFragment = new DatePickerFragment();
        this.firstnameField = (EditText) thisView.findViewById(R.id.new_person_firstname);
        this.nameField = (EditText) thisView.findViewById(R.id.new_person_name);
        this.emailField = (EditText) thisView.findViewById(R.id.new_person_email);
        this.phoneField = (EditText) thisView.findViewById(R.id.new_person_phone);
        this.departementField = (EditText) thisView.findViewById(R.id.new_user_departement);
        this.genderField = (RadioGroup) thisView.findViewById(R.id.new_person_gender);
        this.causeField = (RadioGroup) thisView.findViewById(R.id.new_person_cause);

        final Calendar birthCalendar =  datePickerFragment.getCalendar();
        final AppManager appManager = (AppManager) getActivity();

        //date picker
        datePickerFragment.setHandled(buttonNewBirth);

        //button birth
        buttonNewBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerFragment.show(getFragmentManager(), "Date de naissance");
            }
        });

        //button submit
        buttonNewSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String firstname = firstnameField.getText().toString().trim();
                String name = nameField.getText().toString().trim();
                String email = emailField.getText().toString().trim();
                String phone = phoneField.getText().toString().trim();
                String departement = departementField.getText().toString().trim();
                int selectedGender = genderField.getCheckedRadioButtonId();
                int selectedCause = causeField.getCheckedRadioButtonId();
                Date birth = birthCalendar.getTime();

                Tools.changeStatusEdit(false, firstnameField, appManager);
                Tools.changeStatusEdit(false, nameField, appManager);
                Tools.changeStatusView(false, phoneField, appManager);
                Tools.changeStatusEdit(false, emailField, appManager);
                Tools.changeStatusEdit(false, departementField, appManager);

                //check empty
                if(firstname.isEmpty()){
                    Tools.changeStatusEdit(true, firstnameField, appManager);
                    Tools.textOut(appManager, "Le prénom ne doit pas être vide");
                }
                else if(name.isEmpty()){
                    Tools.changeStatusEdit(true, nameField, appManager);
                    Tools.textOut(appManager, "Le nom ne doit pas être vide");
                }
                else if(email.isEmpty()){
                    Tools.changeStatusEdit(true, emailField, appManager);
                    Tools.textOut(appManager, "L'email ne doit pas être vide");
                }
                else if(!phone.isEmpty()){
                    if(!Pattern.compile("\\d{10}").matcher(phone).matches()){
                        Tools.changeStatusEdit(true, phoneField, appManager);
                        Tools.textOut(appManager, "Le numéro doit contenir 10 chiffres");
                    }
                }
                else if(!departement.isEmpty()){
                    if(!Pattern.compile("\\d{5}").matcher(departement).matches()){
                        Tools.changeStatusEdit(true, departementField, appManager);
                        Tools.textOut(appManager, "Le code postal doit contenir 5 chiffres");
                    }
                }
                //check regex
                else if(!Pattern.compile(".+@+.*").matcher(email).matches()){
                    Tools.changeStatusEdit(true, emailField, appManager);
                    Tools.textOut(appManager, "L'email est incorrect");
                }
                else{
                    boolean gender = false;
                    boolean cause = false;
                    if(selectedGender == R.id.new_person_female) gender=true;
                    if(selectedCause == R.id.new_cause_retrait) cause=true;


                    Person person = new Person(firstname, name, gender, birth, email, phone, departement, cause);
                    int result = appManager.getHelper().createPerson(person);
                    switch (result) {
                        case -1:
                            Tools.textOut(appManager, "Problème lors de l'ajout");
                            break;
                        case 0:
                            Tools.textOut(appManager, "Compte créé");
                            appManager.setCurrentPerson(person);
                            appManager.getFragmentManager().popBackStackImmediate();
                            appManager.show(AppManager.ROAD_REASON, true);
                            break;
                        case 3:
                            Tools.changeStatusEdit(true, emailField, appManager);
                            break;
                    }
                }
            }
        });


        return thisView;
    }

    @Override
    public void onResume() {
        super.onResume();
        //clear field
        firstnameField.getText().clear();
        nameField.getText().clear();
        emailField.getText().clear();
        phoneField.getText().clear();
        genderField.check(R.id.new_person_male);
        causeField.check(R.id.new_cause_suspension);
    }
}
