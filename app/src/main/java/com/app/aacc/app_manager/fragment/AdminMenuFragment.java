package com.app.aacc.app_manager.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.app.aacc.app_manager.AppManager;
import com.app.aacc.R;

/**
 * Created by leuks on 03/07/2016.
 */
public class AdminMenuFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.admin_menu_fragment_app_manager, container, false);
        final Button buttonAccount = (Button) thisView.findViewById(R.id.manage_account);
        final Button buttonGameParam = (Button) thisView.findViewById(R.id.manage_game_param);
        final Button buttonEvaluation = (Button) thisView.findViewById(R.id.manage_evaluation);
        final AppManager appManager = (AppManager) getActivity();

        //button account
        buttonAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appManager.show(AppManager.ROAD_MANAGE_ACCOUNT, true);
            }
        });


        //button game param
        buttonGameParam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appManager.show(AppManager.ROAD_MANAGE_GAME_PARAM, true);
            }
        });

        //button evaluation
        buttonEvaluation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appManager.show(AppManager.ROAD_CHOICE_USER, true);
            }
        });

        return thisView;
    }
}
