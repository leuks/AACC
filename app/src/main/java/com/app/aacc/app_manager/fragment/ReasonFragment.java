package com.app.aacc.app_manager.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.app.aacc.app_manager.AppManager;
import com.app.aacc.R;

import Entities.Tools;

/**
 * Created by leuks on 04/07/2016.
 */
public class ReasonFragment extends Fragment {
    private EditText reasonField;
    private EditText commentField;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.reason_fragment_app_manager, container, false);
        final Button submitReasonButton = (Button) thisView.findViewById(R.id.submit_reason);
        this.reasonField = (EditText) thisView.findViewById(R.id.reason_field);
        this.commentField = (EditText) thisView.findViewById(R.id.comment_field);
        final AppManager appManager = (AppManager) getActivity();

        reasonField.requestFocus();

        //button submit
        submitReasonButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String reason = reasonField.getText().toString();
                String comment = commentField.getText().toString();

                appManager.setCurrentReason(reason, comment);
                appManager.show(AppManager.ROAD_CHOOSE_GAME, true);

            }
        });

        return thisView;
    }

    @Override
    public void onResume() {
        super.onResume();
        reasonField.getText().clear();
        commentField.getText().clear();
    }
}
