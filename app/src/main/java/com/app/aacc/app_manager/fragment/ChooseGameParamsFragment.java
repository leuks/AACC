package com.app.aacc.app_manager.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.app.aacc.R;
import com.app.aacc.app_manager.AppManager;

import java.sql.SQLException;
import java.util.List;

import bdd.DBManager;
import bdd.Game;

/**
 * Created by leuks on 16/07/2016.
 */
public class ChooseGameParamsFragment extends Fragment {
    private AppManager appManager;
    private List<Game> gameList;
    private ListView gameListView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.appManager = (AppManager) getActivity();
        DBManager dbManager = appManager.getHelper();

        try {
            gameList = dbManager.getDaoGame().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View thisView = inflater.inflate(R.layout.choose_game_fragment_params_app_manager, container, false);
        this.gameListView = (ListView) thisView.findViewById(R.id.game_param_list_view);

        //list adpater
        this.gameListView.setAdapter(new ListAdapter(appManager, gameList));

        //list listener
        this.gameListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Game game = gameList.get(position);
                switch(game.getName()){
                    case Game.IMAGE:
                        appManager.show(AppManager.ROAD_PARAMS_IMAGE_GAME, true);
                        break;
                    case Game.SIGN:
                        appManager.show(AppManager.ROAD_PARAMS_SIGN_GAME, true);
                        break;
                }
            }
        });

        return thisView;
    }


    // adapter
    private class ListAdapter extends ArrayAdapter {
        private Context context;
        private List<Game> data;

        public ListAdapter(Context context, List<Game> data){
            super(context, -1, data);
            this.context = context;
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.game_item_param_app_manager, parent, false);
            Game game = data.get(position);
            TextView gameTitle = (TextView) row.findViewById(R.id.game_name_field);

            gameTitle.setText(game.getName());

            return row;
        }
    }
}
