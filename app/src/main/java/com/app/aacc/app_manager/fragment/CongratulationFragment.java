package com.app.aacc.app_manager.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.app.aacc.R;
import com.app.aacc.app_manager.AppManager;

/**
 * Created by leuks on 12/09/2016.
 */
public class CongratulationFragment extends Fragment{
    private AppManager appManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appManager = (AppManager) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.congratulation_fragment, container, false);
        Button finishButton = (Button) thisView.findViewById(R.id.button_finish);

        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appManager.showScore(false);
            }
        });
        return thisView;
    }
}
