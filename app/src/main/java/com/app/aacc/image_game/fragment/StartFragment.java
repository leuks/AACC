package com.app.aacc.image_game.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.app.aacc.image_game.ImageGame;
import com.app.aacc.R;

/**
 * Created by leuks on 09/07/2016.
 */
public class StartFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.start_fragment_image_game, container, false);
        final Button buttonPlay = (Button) thisView.findViewById(R.id.play_button_image_game);
        final ImageGame imageGame = (ImageGame) getActivity();

        //button play
        buttonPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageGame.show(ImageGame.ROAD_RULES_TRIAL, true);
            }
        });

        return thisView;
    }
}
