package com.app.aacc.image_game.fragment;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.aacc.commons.classe.LoadBitmapThread;
import com.app.aacc.image_game.ImageGame;
import com.app.aacc.R;

import Entities.BitmapUser;

/**
 * Created by leuks on 09/07/2016.
 */
public class GameFragment extends Fragment implements BitmapUser{
    private ImageGame imageGame;
    private ImageView imageViewGame;
    private boolean used;
    private int count;
    private Bitmap nextPicture;
    private boolean first;
    private Handler handler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.used=false;
        this.imageGame = (ImageGame) getActivity();
        this.nextPicture = null;
        first = true;
        handler = new Handler();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.game_fragment_image_game, container, false);
        this.imageViewGame = (ImageView) thisView.findViewById(R.id.game_image_view);


        return thisView;
    }


    public void start(){

        if(used){
            display();
            used=false;
        }
        else{
            imageViewGame.setImageBitmap(null);
            Drawable d = getResources().getDrawable(getResources().getIdentifier("l"+count, "drawable", imageGame.getPackageName()), imageGame.getTheme());
            LoadBitmapThread loadBitmapThread = new LoadBitmapThread(this, d);
            loadBitmapThread.execute();
        }
    }

    private void display(){
        final int timeDemand = Integer.parseInt(imageGame.getParameters().get("time"));
        handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {

                if(first){
                    handler.postDelayed(this, timeDemand);
                    first = false;
                }
                else{
                    imageGame.show(ImageGame.ROAD_ANSWER, false);
                    first=true;
                    handler.removeCallbacks(this);
                }
            }

        };
        handler.postDelayed(runnable, 0);
    }

    @Override
    public void onResume() {
        super.onResume();
        imageViewGame.setImageBitmap(nextPicture);
        start();
    }

    @Override
    public void setBitmap(Bitmap bitmap) {
        setNextPicture(bitmap);
        used=true;
        imageGame.show(ImageGame.ROAD_TIMER, false);
    }

    //GS

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public boolean isUsed() {
        return used;
    }

    public void setNextPicture(Bitmap nextPicture) {
        this.nextPicture = nextPicture;
    }

}
