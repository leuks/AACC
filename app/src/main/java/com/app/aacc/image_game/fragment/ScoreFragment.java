package com.app.aacc.image_game.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import com.app.aacc.R;
import com.app.aacc.app_manager.AppManager;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Entities.Accord;
import bdd.Game;

/**
 * Created by leuks on 13/07/2016.
 */
public class ScoreFragment extends Fragment {
    //image game
    private ArrayList<Integer> scores;
    private TextView scoreTextView;
    private ListView scoreListView;
    private Switch switchScoreValidation;
    private AppManager gameActivity;
    private ArrayAdapter<Integer> adapter;
    private double scoreValue;
    private int wanted;


    //sign game

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gameActivity = (AppManager) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.score_fragment,container, false);
        this.scoreTextView = (TextView) thisView.findViewById(R.id.label_score);
        this.scoreListView = (ListView) thisView.findViewById(R.id.score_list_view);
        final Button submitScoreButton = (Button) thisView.findViewById(R.id.submit_score_validation);
        this.switchScoreValidation = (Switch) thisView.findViewById(R.id.validation_switch);


        //listView
        this.adapter = new ListAdapter((Activity) gameActivity, scores);
        this.scoreListView.setAdapter(adapter);
        this.adapter.notifyDataSetChanged();

        //init
        displayWithColor();

        //submit button
        submitScoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gameActivity.showScore(switchScoreValidation.isChecked());
            }
        });

        return thisView;
    }

    public void displayWithColor(){
        if(scoreValue >= wanted){
            this.scoreTextView.setTextColor(Color.GREEN);
            this.switchScoreValidation.setChecked(true);
        }
        else{
            this.scoreTextView.setTextColor(Color.RED);
            this.switchScoreValidation.setChecked(false);
        }
        this.scoreTextView.setText(String.valueOf(scoreValue)+"%");
    }


    // adapter
    private class ListAdapter extends ArrayAdapter {
        private Context context;
        private List<Integer> data;

        public ListAdapter(Context context, List<Integer> data){
            super(context, -1, data);
            this.context = context;
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.score_item, parent, false);
            TextView score = (TextView) row.findViewById(R.id.item_score);

            score.setText(String.valueOf(position+1)+" - "+String.valueOf(data.get(position)+"%"));

            if(data.get(position) >= wanted) score.setTextColor(Color.GREEN);
            else score.setTextColor(Color.RED);

            return row;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        displayWithColor();
    }

    //GS
    public void setScore(double score, ArrayList scores, int wanted){
        this.scoreValue = score;
        this.scores = scores;
        this.wanted = wanted;
    }



}
