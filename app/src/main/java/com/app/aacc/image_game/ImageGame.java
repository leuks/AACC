package com.app.aacc.image_game;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;

import com.app.aacc.R;
import com.app.aacc.image_game.fragment.ScoreFragment;
import com.app.aacc.image_game.fragment.AnswerFragment;
import com.app.aacc.image_game.fragment.GameFragment;
import com.app.aacc.commons.fragment.RulesFragment;
import com.app.aacc.commons.fragment.TimerFragment;
import com.app.aacc.image_game.fragment.StartFragment;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;

import java.util.ArrayList;
import java.util.HashMap;

import Entities.Accord;
import Entities.Tools;
import bdd.DBManager;
import bdd.Game;

/**
 * Created by leuks on 09/07/2016.
 */
public class ImageGame extends OrmLiteBaseActivity<DBManager> implements Accord{
    private StartFragment startFragment;
    private GameFragment gameFragment;
    private TimerFragment timerFragment;
    private RulesFragment rulesFragment;
    private AnswerFragment answerFragment;
    private ArrayList<String> goodAnswers;
    private boolean mode;
    private HashMap<String, String> parameters;
    private DBManager dbManager;

    private ArrayList<Integer> scores;

    public static final int ROAD_START = 0;
    public static final int ROAD_RULES_TRIAL = 1;
    public static final int ROAD_RULES_PLAY = 2;
    public static final int ROAD_TIMER = 3;
    public static final int ROAD_GAME = 4;
    public static final int ROAD_ANSWER = 5;

    public static final int IMAGE_COUNT = 19;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("actualCount", gameFragment.getCount());
        outState.putBoolean("actualUsed", gameFragment.isUsed());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();

        if(count == 0){
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();
        }
        else if(count == 1){
            super.onBackPressed();
        }
        else{
            if(mode){
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        setContentView(R.layout.activity_image_game);

        this.dbManager = getHelper();
        dbManager.initDao();
        this.startFragment = new StartFragment();
        this.gameFragment = new GameFragment();
        this.timerFragment = new TimerFragment();
        this.answerFragment = new AnswerFragment();
        this.parameters = dbManager.getParameters(Game.IMAGE);
        this.scores = new ArrayList();
        mode = false;
        initAnswers();

        //rules fragment
        this.rulesFragment = new RulesFragment();
        Bundle bundleRules = new Bundle();
        bundleRules.putString("game", Game.IMAGE);
        this.rulesFragment.setArguments(bundleRules);

        getHelper().initDao();

        if (savedInstanceState != null) {
            if(savedInstanceState.get("actualCount") != null){
                gameFragment.setCount((Integer) savedInstanceState.get("actualCount"));
                gameFragment.setUsed((Boolean) savedInstanceState.get("actualUsed"));
            }
            return;
        }

        getFragmentManager().beginTransaction().add(R.id.frameLayoutImageGame, startFragment).commit();
    }

    private void init(){
        scores = new ArrayList<>();
    }

    public void show(int direction, boolean back) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        switch (direction) {
            case ROAD_START:
                fragmentTransaction.replace(R.id.frameLayoutImageGame, startFragment);
                this.scores.clear();
                break;
            case ROAD_RULES_TRIAL:
                fragmentTransaction.replace(R.id.frameLayoutImageGame, rulesFragment);
                this.mode = true;
                gameFragment.setCount(18);//for test
                init();
                break;
            case ROAD_RULES_PLAY:
                fragmentTransaction.replace(R.id.frameLayoutImageGame, rulesFragment);
                this.mode = false;
                gameFragment.setCount(1);//for test
                init();
                break;
            case ROAD_TIMER:
                fragmentTransaction.replace(R.id.frameLayoutImageGame, timerFragment);
                break;
            case ROAD_GAME:
                fragmentTransaction.replace(R.id.frameLayoutImageGame, gameFragment);
                break;
            case ROAD_ANSWER:
                fragmentTransaction.replace(R.id.frameLayoutImageGame, answerFragment);
                break;
        }
            if (back) fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

    }

    @Override
    public void timerNotify() {
        show(ROAD_GAME, false);
    }

    @Override
    public void rulesNotify() {
        show(ROAD_GAME, true);
    }


    private void initAnswers(){
        this.goodAnswers = new ArrayList();
        this.goodAnswers.add("0,0,1,1,0");
        this.goodAnswers.add("0,0,0,1,0");
        this.goodAnswers.add("0,0,1,0,0");
        this.goodAnswers.add("1,1,1,0,1");
        this.goodAnswers.add("1,0,1,1,0");
        this.goodAnswers.add("1,1,1,0,1");
        this.goodAnswers.add("1,1,1,1,1");
        this.goodAnswers.add("1,0,1,0,1");
        this.goodAnswers.add("0,0,1,1,1");
        this.goodAnswers.add("0,0,1,1,0");
        this.goodAnswers.add("0,0,1,1,0");
        this.goodAnswers.add("0,0,1,1,0");
        this.goodAnswers.add("1,0,1,1,0");
        this.goodAnswers.add("0,0,0,1,0");
        this.goodAnswers.add("0,1,1,1,0");
        this.goodAnswers.add("0,0,1,1,0");
        this.goodAnswers.add("1,1,1,0,1");
        this.goodAnswers.add("1,1,1,1,1");
        this.goodAnswers.add("1,0,1,1,1");
    }

    public void addScore(ArrayList<Integer> responses){
        int actualCount = gameFragment.getCount();
        String goodRep = this.goodAnswers.get(actualCount-1);
        String[] arrayToTest = goodRep.split(",");
        int calCount=0;
        for(int i=0;i<5;i++){
            if(responses.get(i) == Integer.parseInt(arrayToTest[i])){
                calCount+=20;
            }
        }
        this.scores.add(calCount);
        if(actualCount != IMAGE_COUNT){
            this.gameFragment.setCount(actualCount+1);
            show(ROAD_GAME, false);
        }
        else{

            testFinish();
        }
    }

    private void testFinish(){
        int sum = 0;
        for(int score : scores){
            sum+=score;
        }
        int finalResult = sum/scores.size();

        if(mode){

            if(finalResult >= Integer.parseInt(parameters.get("score"))){
                Tools.textOutLong(this, "Entrainement satisfaisant");
                show(ROAD_RULES_PLAY, false);
            }
            else{
                Tools.textOutLong(this, "Entrainement insatisfaisant");
                show(ROAD_RULES_TRIAL, false);
            }
        }
        else {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("name", Game.IMAGE);
            returnIntent.putIntegerArrayListExtra("scores", scores);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
    }

    //GS


    public ArrayList<Integer> getScores() {
        return scores;
    }

    public void setScores(ArrayList<Integer> scores) {
        this.scores = scores;
    }

    public HashMap<String, String> getParameters(){
        return parameters;
    }

    public boolean isMode() {
        return mode;
    }
}
