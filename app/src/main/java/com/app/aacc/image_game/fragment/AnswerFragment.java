package com.app.aacc.image_game.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ToggleButton;

import com.app.aacc.image_game.ImageGame;
import com.app.aacc.R;

import java.util.ArrayList;

/**
 * Created by leuks on 11/07/2016.
 */
public class AnswerFragment extends Fragment {
    private ToggleButton buttonPieton;
    private ToggleButton button2Roue;
    private ToggleButton buttonVoiture;
    private ToggleButton buttonPanneaux;
    private ToggleButton buttonFeu;

    private ImageGame imageGame;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageGame = (ImageGame) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.answer_fragment_image_game, container, false);
        final Button buttonSubmitAnswers = (Button) thisView.findViewById(R.id.submit_answers_image_game);
        this.buttonPieton = (ToggleButton) thisView.findViewById(R.id.but_pietons);
        this.button2Roue = (ToggleButton) thisView.findViewById(R.id.but_deuxroues);
        this.buttonVoiture = (ToggleButton) thisView.findViewById(R.id.but_voiture);
        this.buttonPanneaux = (ToggleButton) thisView.findViewById(R.id.but_panneaux);
        this.buttonFeu = (ToggleButton) thisView.findViewById(R.id.but_feux);


        //button submit
        buttonSubmitAnswers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean pieton = buttonPieton.isChecked();
                boolean deuxRoues = button2Roue.isChecked();
                boolean voiture = buttonVoiture.isChecked();
                boolean panneau = buttonPanneaux.isChecked();
                boolean feu = buttonFeu.isChecked();
                ArrayList<Integer> responses = new ArrayList<Integer>();
                responses.add(pieton ? 1:0);
                responses.add(deuxRoues ? 1:0);
                responses.add(voiture ? 1:0);
                responses.add(panneau ? 1:0);
                responses.add(feu ? 1:0);
                imageGame.addScore(responses);
            }
        });

        //toogles
        buttonPieton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!buttonPieton.isChecked()) buttonPieton.setBackground(getResources().getDrawable(R.drawable.man_black, imageGame.getTheme()));
                else buttonPieton.setBackground(getResources().getDrawable(R.drawable.man_green, imageGame.getTheme()));
            }
        });
        button2Roue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!button2Roue.isChecked()) button2Roue.setBackground(getResources().getDrawable(R.drawable.bike_black, imageGame.getTheme()));
                else button2Roue.setBackground(getResources().getDrawable(R.drawable.bike_green, imageGame.getTheme()));
            }
        });
        buttonVoiture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!buttonVoiture.isChecked()) buttonVoiture.setBackground(getResources().getDrawable(R.drawable.car_black, imageGame.getTheme()));
                else buttonVoiture.setBackground(getResources().getDrawable(R.drawable.car_green, imageGame.getTheme()));
            }
        });
        buttonPanneaux.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!buttonPanneaux.isChecked()) buttonPanneaux.setBackground(getResources().getDrawable(R.drawable.sign_black, imageGame.getTheme()));
                else buttonPanneaux.setBackground(getResources().getDrawable(R.drawable.sign_green, imageGame.getTheme()));
            }
        });
        buttonFeu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!buttonFeu.isChecked()) buttonFeu.setBackgroundDrawable(getResources().getDrawable(R.drawable.fire_black, imageGame.getTheme()));
                else buttonFeu.setBackground(getResources().getDrawable(R.drawable.fire_green, imageGame.getTheme()));
            }
        });

        return thisView;
    }

    @Override
    public void onResume() {
        super.onResume();
        this.buttonPieton.setChecked(false);
        this.button2Roue.setChecked(false);
        this.buttonFeu.setChecked(false);
        this.buttonPanneaux.setChecked(false);
        this.buttonVoiture.setChecked(false);
    }
}
