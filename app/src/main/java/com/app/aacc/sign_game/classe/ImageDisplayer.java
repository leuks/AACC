package com.app.aacc.sign_game.classe;

import android.app.Fragment;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.Button;

import com.app.aacc.R;
import com.app.aacc.sign_game.fragment.GameFragment;

import Entities.Tuple;

/**
 * Created by leuks on 25/07/2016.
 */
public class ImageDisplayer{

    private Tuple<Boolean, Button> tuple;
    private int duration;
    private GameFragment parent;
    private int background;
    private boolean show;

    public ImageDisplayer(Tuple<Boolean, Button> tuple, int duration, GameFragment parent,int background ){
        this.tuple = tuple;
        this.duration = duration;
        this.parent = parent;
        this.background = background;
        this.show = false;
    }

    public void start() {
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {

                onProgressUpdate(background);
                Log.i("image","tick");
                if(show){
                    onPostExecute();
                    handler.removeCallbacks(this);
                }
                else{
                    show = true;
                    handler.postDelayed(this, duration);
                }
            }
        };

        handler.postDelayed(runnable, 0);
    }

    private void onProgressUpdate(Integer ... values) {
        try{
            tuple.getArg2().setBackground(parent.getResources().getDrawable(values[0], parent.getActivity().getTheme()));
        }catch(IllegalStateException ex){

        }
    }


    private void onPostExecute() {
        tuple.getArg2().setBackgroundColor(Color.TRANSPARENT);
        tuple.setArg1(false);
    }
}
