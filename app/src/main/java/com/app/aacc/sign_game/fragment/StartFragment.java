package com.app.aacc.sign_game.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.app.aacc.R;
import com.app.aacc.image_game.ImageGame;
import com.app.aacc.sign_game.SignGame;

/**
 * Created by leuks on 09/07/2016.
 */
public class StartFragment extends Fragment {
    private SignGame signGame;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        signGame = (SignGame) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.start_fragment_sign_game, container, false);
        final Button buttonPlay = (Button) thisView.findViewById(R.id.play_button_sign_game);


        //button play
        buttonPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signGame.show(ImageGame.ROAD_RULES_TRIAL, true);
            }
        });

        return thisView;
    }
}
