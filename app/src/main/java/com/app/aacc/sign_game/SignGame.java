package com.app.aacc.sign_game;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;

import com.app.aacc.R;
import com.app.aacc.commons.fragment.RulesFragment;
import com.app.aacc.sign_game.fragment.ScoreFragment;
import com.app.aacc.commons.fragment.TimerFragment;
import com.app.aacc.sign_game.fragment.GameFragment;
import com.app.aacc.sign_game.fragment.StartFragment;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;

import java.util.ArrayList;
import java.util.HashMap;

import Entities.Accord;
import Entities.Tools;
import bdd.DBManager;
import bdd.Game;

/**
 * Created by leuks on 22/07/2016.
 */
public class SignGame extends OrmLiteBaseActivity<DBManager> implements Accord {
    private RulesFragment rulesFragment;
    private StartFragment startFragment;
    private GameFragment gameFragment;
    private TimerFragment timerFragment;
    private HashMap<String, String> parameters;
    private DBManager dbManager;
    private int displayerCount;
    private int displayerErrCount;
    private AudioManager audioManager;

    private int nbGood;
    private int nbBad;
    private long moyTime;

    private boolean mode;

    public static final int ROAD_START = 0;
    public static final int ROAD_RULES_TRIAL = 1;
    public static final int ROAD_RULES_PLAY = 2;
    public static final int ROAD_GAME = 3;
    public static final int ROAD_TIMER = 4;
    public static final int ROAD_SCORE = 5;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean res = super.onKeyDown(keyCode, event);
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            gameFragment.setVolume(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
        }
        return res;
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        boolean res = super.onKeyDown(keyCode, event);
        if(keyCode == KeyEvent.KEYCODE_VOLUME_UP){
            gameFragment.setVolume(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
        }
        return res;
    }

    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();
        if(count == 0){
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();
        }
        else if(count == 1){
            super.onBackPressed();
        }
        else{
            if(mode){
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        setContentView(R.layout.activity_sign_game);
        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);

        dbManager = getHelper();
        dbManager.initDao();
        startFragment = new StartFragment();
        gameFragment = new GameFragment();
        timerFragment = new TimerFragment();
        parameters = dbManager.getParameters(Game.SIGN);
        displayerCount = 0;
        displayerErrCount = 0;
        nbGood=0;
        nbBad=0;
        moyTime=0;

        //audio
        audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        gameFragment.setVolume(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));

        //rules fragment
        this.rulesFragment = new RulesFragment();
        Bundle bundleRules = new Bundle();
        bundleRules.putString("game", Game.SIGN);
        this.rulesFragment.setArguments(bundleRules);


        if (savedInstanceState != null) {
            return;
        }

        getFragmentManager().beginTransaction().add(R.id.frameLayoutSignGame, startFragment).commit();
    }


    public void show(int direction, boolean back){
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        switch (direction) {
            case ROAD_START:
                fragmentTransaction.replace(R.id.frameLayoutSignGame, startFragment);
                break;
            case ROAD_RULES_TRIAL:
                fragmentTransaction.replace(R.id.frameLayoutSignGame, rulesFragment);
                this.mode = true;
                break;
            case ROAD_RULES_PLAY:
                fragmentTransaction.replace(R.id.frameLayoutSignGame, rulesFragment);
                this.mode = false;
                break;
            case ROAD_GAME:
                gameFragment.init();
                fragmentTransaction.replace(R.id.frameLayoutSignGame, gameFragment);
                break;
            case ROAD_TIMER:
                fragmentTransaction.replace(R.id.frameLayoutSignGame, timerFragment);
                break;
        }

        if (back) fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void timerNotify() {
        show(ROAD_GAME, false);
    }

    @Override
    public void rulesNotify() {
        show(ROAD_TIMER, true);
    }


    public void gameTerminated(){
        nbGood = gameFragment.getCountGreen()+gameFragment.getCountBlue();
        nbBad = gameFragment.getCountRed();

        long lSum = 0;
        ArrayList<Long> times = gameFragment.getTimes();
        for(Long l : times){
            lSum+=l;
        }

        if(times.size() != 0){
            moyTime = lSum / times.size();
        }
        else{
            moyTime = 0;
        }

         if(isMode()){
             if((nbGood >= (displayerCount*(Double.parseDouble(parameters.get("score_good"))/100))) && (nbBad <= (displayerErrCount*(Double.parseDouble(parameters.get("score_bad"))/100)))){
                 Tools.textOutLong(this, "Entrainement satisfaisant");
                 show(ROAD_RULES_PLAY, false);
             }
             else{
                 Tools.textOutLong(this, "Entrainement insatisfaisant");
                 show(ROAD_RULES_TRIAL, false);
             }
        }
        else{
             exitGame();
         }
    }

    private void exitGame(){
        Intent returnIntent = new Intent();
        returnIntent.putExtra("name", Game.SIGN);
        returnIntent.putExtra("score_good", nbGood);
        returnIntent.putExtra("score_bad", nbBad);
        returnIntent.putExtra("nb_good", displayerCount);
        returnIntent.putExtra("nb_bad", displayerErrCount);
        returnIntent.putExtra("moy_time", moyTime);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    //GS

    public HashMap<String, String> getParameters() {
        return parameters;
    }

    public GameFragment getGameFragment() {
        return gameFragment;
    }

    public void incDisplayerCount() {
        displayerCount++;
    }

    public void incErrDisplayerCount() {
        displayerErrCount++;
    }

    public boolean isMode() {
        return mode;
    }


    public void setDisplayerCount(int displayerCount) {
        this.displayerCount = displayerCount;
    }

    public void setDisplayerErrCount(int displayerErrCount) {
        this.displayerErrCount = displayerErrCount;
    }

    public int getDisplayerCount() {
        return displayerCount;
    }

    public int getDisplayerErrCount() {
        return displayerErrCount;
    }
}
