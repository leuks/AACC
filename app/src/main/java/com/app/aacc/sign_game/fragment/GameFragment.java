package com.app.aacc.sign_game.fragment;

import android.app.Fragment;
import android.content.Context;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

import com.app.aacc.R;
import com.app.aacc.sign_game.SignGame;
import com.app.aacc.sign_game.classe.GameEngine;
import com.app.aacc.sign_game.classe.ImageDisplayer;

import java.text.DecimalFormat;
import java.util.ArrayList;

import Entities.Tools;
import Entities.Tuple;

/**
 * Created by leuks on 22/07/2016.
 */
public class GameFragment extends Fragment{
    private SignGame signGame;
    private ArrayList<Tuple<Boolean, Button>> tuples;
    private ArrayList<Long> times;
    private Tuple<Boolean, Button> lastClickedButton;
    private long lastTime;
    private Chronometer chronometer;
    private int countRed;
    private int countGreen;
    private int countBlue;
    private GameEngine gameEngine;
    private ToneGenerator toneG;
    private boolean velocityChanged;
    private boolean pause;
    private long timeWhenStop;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        signGame = (SignGame) getActivity();
        tuples = new ArrayList<>();
        times = new ArrayList<>();
        lastClickedButton = new Tuple(false, new Button(signGame));
        lastTime = 0;
        countRed = 0;
        countGreen = 0;
        countBlue = 0;
        gameEngine = new GameEngine(signGame);
        velocityChanged = false;
        signGame.setDisplayerCount(0);
        signGame.setDisplayerErrCount(0);
        pause = false;
        timeWhenStop = 0;
    }

    public void init(){
        if(signGame != null) {
            lastTime = 0;
            countRed = 0;
            countGreen = 0;
            countBlue = 0;
            gameEngine = new GameEngine(signGame);
            velocityChanged = false;
            signGame.setDisplayerCount(0);
            signGame.setDisplayerErrCount(0);
            pause = false;
            timeWhenStop = 0;
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.game_fragment_sign_game, container, false);
        final Button redButton = (Button) thisView.findViewById(R.id.but_red);
        final Button greenButton = (Button) thisView.findViewById(R.id.but_green);
        final Button blueButton = (Button) thisView.findViewById(R.id.but_blue);
        final Button pauseButton = (Button) thisView.findViewById(R.id.but_pause);
        chronometer = (Chronometer) thisView.findViewById(R.id.chrono);


        //button pause
        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pause){
                    Tools.textOut(signGame, "Reprise du jeu");
                    gameEngine.setCanRun(true);
                    pause = false;
                    pauseButton.setBackground(getResources().getDrawable(R.drawable.pause, signGame.getTheme()));
                    startChrono(SystemClock.elapsedRealtime() + timeWhenStop);
                }
                else{
                    Tools.textOut(signGame, "Le jeu est désormais en pause");
                    gameEngine.setCanRun(false);
                    pause = true;
                    pauseButton.setBackground(getResources().getDrawable(R.drawable.play, signGame.getTheme()));
                    stopChrono();
                }
            }
        });


        //chrono
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                long elapsedMillis = SystemClock.elapsedRealtime() - chronometer.getBase();

                int wantedMillis;
                int displayTime;
                if(signGame.isMode()){
                    wantedMillis = (int) 1*60*1000;
                    displayTime = 1300;
                }
                else{
                    wantedMillis= (int) Double.parseDouble(signGame.getParameters().get("total_time"))*60*1000;
                    displayTime = Integer.parseInt(signGame.getParameters().get("display_time"));
                }


                double temp = displayTime*0.8;
                gameEngine.setStep(displayTime);

                
                if(elapsedMillis >= wantedMillis){
                    stopChrono();
                    gameEngine.onPostExecute();
                }
                else if(elapsedMillis >= (wantedMillis/2)){
                    if(!velocityChanged){
                        toneG.startTone(ToneGenerator.TONE_CDMA_EMERGENCY_RINGBACK, 200);
                        gameEngine.setStep((int) temp);
                        velocityChanged = true;
                    }
                }
            }
        });

        //add buttons && listener
        tuples.add(new Tuple(false, (Button) thisView.findViewById(R.id.case1)));
        tuples.add(new Tuple(false, (Button) thisView.findViewById(R.id.case2)));
        tuples.add(new Tuple(false, (Button) thisView.findViewById(R.id.case3)));
        tuples.add(new Tuple(false, (Button) thisView.findViewById(R.id.case4)));
        tuples.add(new Tuple(false, (Button) thisView.findViewById(R.id.case5)));
        tuples.add(new Tuple(false, (Button) thisView.findViewById(R.id.case6)));

        for(final Tuple<Boolean, Button> tuple : tuples){
            final Button button = tuple.getArg2();
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastClickedButton = tuple;
                    lastTime = System.currentTimeMillis();

                    if(button.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.red, signGame.getTheme()).getConstantState())){
                        countRed++;
                        redButton.setText(String.valueOf(countRed));
                        times.add(System.currentTimeMillis()-lastTime);
                        toneG.startTone(ToneGenerator.TONE_CDMA_SOFT_ERROR_LITE, 100);
                    }

                }
            });
        }

        //palette listener
        greenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button last = lastClickedButton.getArg2();
                boolean isVisible = lastClickedButton.getArg1();

                if (isVisible) {
                    if (last.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.green, signGame.getTheme()).getConstantState())) {
                        countGreen++;
                        greenButton.setText(String.valueOf(countGreen));
                        times.add(System.currentTimeMillis() - lastTime);
                        toneG.startTone(ToneGenerator.TONE_CDMA_CONFIRM, 100);
                    }
                }

            }
        });

        blueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button last = lastClickedButton.getArg2();
                boolean isVisible = lastClickedButton.getArg1();

                if (isVisible) {
                    if (last.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.blue, signGame.getTheme()).getConstantState())) {
                        countBlue++;
                        blueButton.setText(String.valueOf(countBlue));
                        times.add(System.currentTimeMillis() - lastTime);
                        toneG.startTone(ToneGenerator.TONE_CDMA_CONFIRM, 100);
                    }
                }

            }
        });

        start();

        return thisView;
    }

    //chrono
    public void startChrono(long wTime){
        if(wTime == 0) chronometer.setBase(SystemClock.elapsedRealtime());
        else chronometer.setBase(wTime);

        chronometer.start();
    }

    public void stopChrono(){
        timeWhenStop = chronometer.getBase()-SystemClock.elapsedRealtime();
        Log.i("base", ""+chronometer.getBase());
        Log.i("timeStop", ""+timeWhenStop);
        chronometer.stop();
    }


    private void start(){
        gameEngine.start();
    }

    //GS

    public ArrayList<Tuple<Boolean, Button>> getTuples() {
        return tuples;
    }

    public ArrayList<Long> getTimes() {
        return times;
    }

    public int getCountRed() {
        return countRed;
    }

    public int getCountGreen() {
        return countGreen;
    }

    public Chronometer getChronometer() {
        return chronometer;
    }

    public int getCountBlue() {
        return countBlue;
    }

    public ToneGenerator getToneG() {
        return toneG;
    }

    public void setVolume(int vol){
        toneG = new ToneGenerator(AudioManager.STREAM_MUSIC, vol);
        Toast.makeText(signGame, ""+vol, Toast.LENGTH_SHORT).show();
    }
}
