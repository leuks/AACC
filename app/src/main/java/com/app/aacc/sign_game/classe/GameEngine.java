package com.app.aacc.sign_game.classe;

import android.app.Fragment;
import android.graphics.Color;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.Button;

import com.app.aacc.R;
import com.app.aacc.sign_game.SignGame;
import com.app.aacc.sign_game.fragment.GameFragment;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import Entities.Tuple;

/**
 * Created by leuks on 25/07/2016.
 */

public class GameEngine {

    private SignGame signGame;
    private int step;
    private boolean first;
    private Handler handlerGame;
    private RunnableGame runnableGame;
    private Button lastButton;
    private boolean canRun;
    private GameFragment gameFragment;
    private ArrayList<Tuple<Boolean, Button>> tuples;

    public GameEngine(SignGame signGame) {
        this.signGame = signGame;
        lastButton = null;
    }


    public void start() {
        first = true;
        canRun = true;
        gameFragment = signGame.getGameFragment();
        tuples = gameFragment.getTuples();

        handlerGame = new Handler();
        runnableGame = new RunnableGame();

        handlerGame.postDelayed(runnableGame,0);
    }

    private class RunnableGame implements Runnable{
        private volatile boolean run;
        private Handler handlerImage;
        private RunnableImage runnableImage;

        public RunnableGame(){
            run = true;
            handlerImage = new Handler();
            runnableImage = new RunnableImage();
        }

        @Override
        public void run() {
            Log.i("turn"," encore");
            if(canRun) {
                if (first) {
                    gameFragment.startChrono(0);
                    handlerImage.postDelayed(runnableImage, 2000);
                    first = false;
                } else {
                    handlerImage.postDelayed(runnableImage, step);
                }
            }
            else{
                handlerImage.postDelayed(runnableGame, 1000);
            }
        }

        public boolean isRunning() {
            return run;
        }

        public void stop() {
            this.run = false;
            runnableImage.stop();
            handlerImage.removeCallbacksAndMessages(runnableImage);
        }
    }


    private class RunnableImage implements Runnable {
        private volatile boolean run;

        public RunnableImage(){
            run = true;
        }

        @Override
        public void run() {

            if(!run) return;

            int rand =  (int) (Math.random() * (tuples.size()));
            Tuple<Boolean, Button> tuple = tuples.get(rand);

            Log.i("game" +
                    "","tick");

            while(tuple.getArg1() || (tuple.getArg2().equals(lastButton))){
                rand =  (int) (Math.random() *(tuples.size()));
                tuple = tuples.get(rand);
            }

            int chooseBkg = chooseRandomBackground();

            if(chooseBkg == R.drawable.red) signGame.incErrDisplayerCount();
            else signGame.incDisplayerCount();


            ImageDisplayer imageDisplayer = new ImageDisplayer(tuple, step, gameFragment, chooseBkg);
            lastButton = tuple.getArg2();
            imageDisplayer.start();

            tuple.setArg1(true);

            handlerGame.postDelayed(runnableGame, 0);
        }

        public boolean isRunning() {
            return run;
        }

        public void stop() {
            this.run = false;
        }
    };

    public void onPostExecute() {
        runnableGame.stop();
        handlerGame.removeCallbacksAndMessages(runnableGame);
        signGame.gameTerminated();
    }

    private int chooseRandomBackground(){
        double rand = 1 + (Math.random() *(4-1));
        int drawableID=0;
        switch((int) rand){
            case 1:
                drawableID = R.drawable.red;
                break;
            case 2:
                drawableID = R.drawable.blue;
                break;
            case 3:
                drawableID = R.drawable.green;
                break;
        }

        return drawableID;
    }


    //GS
    public void setStep(int step){
        this.step = step;
    }

    public void setCanRun(boolean canRun) {
        this.canRun = canRun;
    }
}