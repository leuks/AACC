package com.app.aacc.sign_game.fragment;

        import android.app.Activity;
        import android.app.Fragment;
        import android.content.Context;
        import android.graphics.Color;
        import android.os.Bundle;
        import android.support.annotation.Nullable;
        import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ArrayAdapter;
        import android.widget.Button;
        import android.widget.CompoundButton;
        import android.widget.ListView;
        import android.widget.Switch;
        import android.widget.TextView;

        import com.app.aacc.R;
        import com.app.aacc.app_manager.AppManager;


        import java.lang.reflect.Array;
        import java.util.ArrayList;
        import java.util.Arrays;
        import java.util.HashMap;
        import java.util.List;

        import Entities.Accord;

/**
 * Created by leuks on 13/07/2016.
 */
public class ScoreFragment extends Fragment {
    //image game
    private ArrayList<Integer> scores;
    private TextView scoreTextView;
    private ListView scoreListView;
    private Switch switchScoreValidation;
    private AppManager gameActivity;
    private ArrayAdapter<Integer> adapter;
    private List<Integer> validators;
    private double scoreValue;
    private int total;
    private int totalErr;
    private HashMap<Integer, Integer> map;
    private boolean correct;


    //sign game

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gameActivity = (AppManager) getActivity();
        correct = true;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.score_fragment,container, false);
        this.scoreTextView = (TextView) thisView.findViewById(R.id.label_score);
        this.scoreListView = (ListView) thisView.findViewById(R.id.score_list_view);
        final Button submitScoreButton = (Button) thisView.findViewById(R.id.submit_score_validation);
        this.switchScoreValidation = (Switch) thisView.findViewById(R.id.validation_switch);


        //listView
        this.adapter = new ListAdapter((Activity) gameActivity, scores);
        this.scoreListView.setAdapter(adapter);
        this.adapter.notifyDataSetChanged();

        //init
        displayWithColor();

        //submit button
        submitScoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gameActivity.showScore(switchScoreValidation.isChecked());
            }
        });


        return thisView;
    }

    public void displayWithColor(){
        this.scoreTextView.setText(String.valueOf(scoreValue)+"ms");
    }


    // adapter
    private class ListAdapter extends ArrayAdapter {
        private Context context;
        private List<Integer> data;

        public ListAdapter(Context context, List<Integer> data){
            super(context, -1, data);
            this.context = context;
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.score_item, parent, false);
            TextView score = (TextView) row.findViewById(R.id.item_score);
            if(position==0){
                score.setText("Bons: "+data.get(position)+"/"+total);
                if(data.get(position) >= (total*(((double) validators.get(position))/100))){
                    score.setTextColor(Color.GREEN);
                }
                else{
                    score.setTextColor(Color.RED);
                    correct = false;
                }
            }
            else if(position ==1){
                score.setText("Erreurs: "+data.get(position)+"/"+totalErr);
                if(data.get(position) <= (totalErr*(((double) validators.get(position))/100))){
                    score.setTextColor(Color.GREEN);
                }
                else{
                    score.setTextColor(Color.RED);
                    correct = false;
                }
            }

            if(correct){
                switchScoreValidation.setChecked(true);
            }else{
                switchScoreValidation.setChecked(false);
            }

            return row;
        }
    }

    //GS

    public void setScore(double moyScore,ArrayList vals, ArrayList validators, int total, int totalErr){
        this.scoreValue = moyScore;
        this.scores = vals;
        this.validators = validators;
        this.total = total;
        this.totalErr = totalErr;
    }


}
