package com.app.aacc.commons.classe;

/**
 * Created by leuks on 23/07/2016.
 */

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import Entities.BitmapUser;
import Entities.Tuple;

public class LoadBitmapThread extends AsyncTask<Void, Tuple<ByteArrayInputStream, BitmapFactory.Options>, Tuple<ByteArrayInputStream, BitmapFactory.Options>> {
    private Fragment parent;
    private Drawable currentDrawable;

    public LoadBitmapThread(Fragment parent, Drawable currentDrawable){
        this.parent=parent;
        this.currentDrawable=currentDrawable;
    }

    protected Tuple<ByteArrayInputStream, BitmapFactory.Options> doInBackground(Void... var1) {
        final int REQUIRED_SIZE=70;
        Bitmap b = null;

        //Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;

        BitmapDrawable bitDw = ((BitmapDrawable) currentDrawable);
        Bitmap bitmap = bitDw.getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] imageInByte = stream.toByteArray();
        ByteArrayInputStream bis = new ByteArrayInputStream(imageInByte);

        int scale = 1;
        if (o.outHeight > REQUIRED_SIZE || o.outWidth > REQUIRED_SIZE) {
            scale = (int) Math.pow(2, (int) Math.ceil(Math.log(REQUIRED_SIZE /
                    (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
        }


        //Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;

        return new Tuple<>(bis, o2);
    }

    protected void onPostExecute(Tuple<ByteArrayInputStream, BitmapFactory.Options> tuple){
        ((BitmapUser) parent).setBitmap(BitmapFactory.decodeStream(tuple.getArg1(), null, tuple.getArg2()));
        try {
            tuple.getArg1().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}