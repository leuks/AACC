package com.app.aacc.commons.fragment;

import android.app.Fragment;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.aacc.R;

import Entities.Accord;

/**
 * Created by leuks on 09/07/2016.
 */
public class TimerFragment extends Fragment {
    private TextView timerText;
    private Accord gameActivity;
    private int count;
    private ToneGenerator toneGenerator;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gameActivity = (Accord) getActivity();
        count=0;
        toneGenerator = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.timer_fragment_image_game, container, false);
        this.timerText = (TextView) thisView.findViewById(R.id.timer_text);
        return thisView;
    }

    private void start() {

        count = 4;
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                count--;

                if(count==0){
                    notifyAccord();
                    handler.removeCallbacks(this);
                }
                else{
                    timerText.setText(String.valueOf(count));
                    handler.postDelayed(this, 1000);
                }
            }
        };

        handler.postDelayed(runnable, 0);
    }


    public void notifyAccord(){
        gameActivity.timerNotify();
    }

    @Override
    public void onResume() {
        super.onResume();
        start();
    }

}
