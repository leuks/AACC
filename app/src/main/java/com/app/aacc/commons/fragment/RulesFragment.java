package com.app.aacc.commons.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.app.aacc.R;

import Entities.Accord;
import bdd.Game;

/**
 * Created by leuks on 09/07/2016.
 */
public class RulesFragment extends Fragment {
    private TextView rulesText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.rules_fragment, container, false);
        final Button buttonOk = (Button) thisView.findViewById(R.id.rules_ok);
        final Accord accordActivity = (Accord) getActivity();
        this.rulesText = (TextView) thisView.findViewById(R.id.rules_text);
        switch(this.getArguments().getString("game")){
            case Game.SIGN:
                this.rulesText.setText(getResources().getString(R.string.sign_game_rules));
                break;
            case Game.IMAGE:
                this.rulesText.setText(getResources().getString(R.string.image_game_rules));
                break;
        }


        //ok button
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accordActivity.rulesNotify();
            }
        });

        return thisView;
    }

}
